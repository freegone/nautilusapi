using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Model.DB;
using Services;
using Services.AutoMapper;
using Services.Interfaces;

namespace Web
{
    public class Startup
    {
        private readonly string nautilusPolicy = "nautilusPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(DomainProfile).Assembly);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Nautilis API", Version = "v1" });
            });

            services.AddScoped<IHomeService, HomeService>();
            services.AddScoped<ICompaniesService, CompaniesService>();
            services.AddScoped<IOfficesServices, OfficesService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<IProgramsService, ProgramsService>();
            services.AddScoped<IConceptsService, ConceptsService>();
            services.AddScoped<IPaymentMethodsService, PaymentMethodsService>();

            var connection = @"Server=DESKTOP-PF4FEQ3\SQLEXPRESS;Database=Nautilus;Trusted_Connection=True;";

            // var connection = @"Server=VMEB67189;Database=Nautilus;User Id=sa;Password=0tTR%9y_CG;";

            services.AddDbContext<NautilusDB>(options => options.UseSqlServer(connection));

            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAgentsService, AgentsService>();
            services.AddScoped<IVendorsService, VendorsService>();
            services.AddScoped<ICustomersService, CustomersService>();
            services.AddScoped<ICostsService, CostsService>();
            services.AddScoped<IDeparmentsService, DepartmentsService>();
            services.AddScoped<ISalesService, SalesService>();
            services.AddScoped<IExpensesService, ExpensesService>();
            services.AddScoped<IBudgetsService, BudgetsService>();

            services.AddCors(o => o.AddPolicy(nautilusPolicy, builder =>
            {
                builder.WithOrigins("http://localhost:4200", "http://162.255.85.250:443")
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1"));
            }

            if (env.IsProduction())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(nautilusPolicy);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireCors(nautilusPolicy);
            });
        }
    }
}
