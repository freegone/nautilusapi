﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Access] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(35) NULL,
        CONSTRAINT [PK_Access] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Companies] (
        [Id] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [RFC] nvarchar(50) NULL,
        [Email] nvarchar(150) NULL,
        [Phone] nvarchar(15) NULL,
        [Address] nvarchar(250) NULL,
        [Notes] nvarchar(300) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        CONSTRAINT [PK_Companies] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Modules] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(150) NULL,
        CONSTRAINT [PK_Modules] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Profiles] (
        [Id] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        CONSTRAINT [PK_Profiles] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Offices] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [Address] nvarchar(250) NULL,
        [Notes] nvarchar(300) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Offices] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Offices_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [ProfilesDetails] (
        [Id] uniqueidentifier NOT NULL,
        [AccessId] int NOT NULL,
        [ProfileId] uniqueidentifier NOT NULL,
        [ModuleId] int NOT NULL,
        [ActionView] bit NOT NULL,
        [ActionAdd] bit NOT NULL,
        [ActionEdit] bit NOT NULL,
        [ActionDelete] bit NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [ProfilesId] uniqueidentifier NULL,
        [ModulesId] int NULL,
        CONSTRAINT [PK_ProfilesDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProfilesDetails_Access_AccessId] FOREIGN KEY ([AccessId]) REFERENCES [Access] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProfilesDetails_Modules_ModulesId] FOREIGN KEY ([ModulesId]) REFERENCES [Modules] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_ProfilesDetails_Profiles_ProfilesId] FOREIGN KEY ([ProfilesId]) REFERENCES [Profiles] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Agents] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] uniqueidentifier NOT NULL,
        [RFC] nvarchar(50) NULL,
        [Phone] nvarchar(15) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Agents] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Agents_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Agents_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Concepts] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Type] int NOT NULL,
        [Name] nvarchar(250) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Concepts] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Concepts_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Concepts_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Costs] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Level1] uniqueidentifier NOT NULL,
        [Level2] uniqueidentifier NOT NULL,
        [Level3] uniqueidentifier NOT NULL,
        [Level4] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Costs] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Costs_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Costs_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Customers] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] uniqueidentifier NOT NULL,
        [RFC] nvarchar(50) NULL,
        [Email] nvarchar(150) NULL,
        [Phone] nvarchar(15) NULL,
        [Address] nvarchar(250) NULL,
        [Notes] nvarchar(300) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Customers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Customers_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Customers_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [PaymentMethods] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_PaymentMethods] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_PaymentMethods_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_PaymentMethods_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Programs] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Programs] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Programs_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Programs_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Projects] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [Units] nvarchar(150) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Projects] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Projects_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Projects_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Users] (
        [Id] uniqueidentifier NOT NULL,
        [ProfileId] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [Email] nvarchar(150) NULL,
        [Phone] nvarchar(15) NULL,
        [Password] nvarchar(15) NULL,
        [Status] bit NOT NULL,
        [Picture] nvarchar(50) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [ProfilesId] uniqueidentifier NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Users_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Users_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Users_Profiles_ProfilesId] FOREIGN KEY ([ProfilesId]) REFERENCES [Profiles] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Vendors] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [Name] uniqueidentifier NOT NULL,
        [RFC] nvarchar(50) NULL,
        [Email] nvarchar(150) NULL,
        [Phone] nvarchar(15) NULL,
        [Address] nvarchar(250) NULL,
        [Notes] nvarchar(300) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_Vendors] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Vendors_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Vendors_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [Departments] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [ProjectId] uniqueidentifier NOT NULL,
        [Unit] uniqueidentifier NOT NULL,
        [Type] nvarchar(150) NULL,
        [Description] nvarchar(250) NULL,
        [SalePrice] nvarchar(150) NULL,
        [Characteristics] nvarchar(150) NULL,
        [Status] bit NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        [ProjectsId] uniqueidentifier NULL,
        CONSTRAINT [PK_Departments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Departments_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Departments_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Departments_Projects_ProjectsId] FOREIGN KEY ([ProjectsId]) REFERENCES [Projects] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE TABLE [UsersDetails] (
        [Id] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [UsersId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        CONSTRAINT [PK_UsersDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UsersDetails_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_UsersDetails_Users_UsersId] FOREIGN KEY ([UsersId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Agents_CompaniesId] ON [Agents] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Agents_OfficesId] ON [Agents] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Concepts_CompaniesId] ON [Concepts] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Concepts_OfficesId] ON [Concepts] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Costs_CompaniesId] ON [Costs] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Costs_OfficesId] ON [Costs] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Customers_CompaniesId] ON [Customers] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Customers_OfficesId] ON [Customers] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Departments_CompaniesId] ON [Departments] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Departments_OfficesId] ON [Departments] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Departments_ProjectsId] ON [Departments] ([ProjectsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Offices_CompaniesId] ON [Offices] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_PaymentMethods_CompaniesId] ON [PaymentMethods] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_PaymentMethods_OfficesId] ON [PaymentMethods] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_ProfilesDetails_AccessId] ON [ProfilesDetails] ([AccessId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_ProfilesDetails_ModulesId] ON [ProfilesDetails] ([ModulesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_ProfilesDetails_ProfilesId] ON [ProfilesDetails] ([ProfilesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Programs_CompaniesId] ON [Programs] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Programs_OfficesId] ON [Programs] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Projects_CompaniesId] ON [Projects] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Projects_OfficesId] ON [Projects] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Users_CompaniesId] ON [Users] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Users_OfficesId] ON [Users] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Users_ProfilesId] ON [Users] ([ProfilesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_UsersDetails_OfficesId] ON [UsersDetails] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_UsersDetails_UsersId] ON [UsersDetails] ([UsersId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Vendors_CompaniesId] ON [Vendors] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    CREATE INDEX [IX_Vendors_OfficesId] ON [Vendors] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210225182244_Initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210225182244_Initial', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210309225515_ChangeStringValueNameInProgramsTable')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Programs]') AND [c].[name] = N'Name');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Programs] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [Programs] ALTER COLUMN [Name] nvarchar(250) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210309225515_ChangeStringValueNameInProgramsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210309225515_ChangeStringValueNameInProgramsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327195624_SolveNamingInAgentsTable')
BEGIN
    DECLARE @var1 sysname;
    SELECT @var1 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[PaymentMethods]') AND [c].[name] = N'Name');
    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [PaymentMethods] DROP CONSTRAINT [' + @var1 + '];');
    ALTER TABLE [PaymentMethods] ALTER COLUMN [Name] nvarchar(250) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327195624_SolveNamingInAgentsTable')
BEGIN
    DECLARE @var2 sysname;
    SELECT @var2 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Agents]') AND [c].[name] = N'Name');
    IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Agents] DROP CONSTRAINT [' + @var2 + '];');
    ALTER TABLE [Agents] ALTER COLUMN [Name] nvarchar(250) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327195624_SolveNamingInAgentsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327195624_SolveNamingInAgentsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    DECLARE @var3 sysname;
    SELECT @var3 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Departments]') AND [c].[name] = N'Unit');
    IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [Departments] DROP CONSTRAINT [' + @var3 + '];');
    ALTER TABLE [Departments] ALTER COLUMN [Unit] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    DECLARE @var4 sysname;
    SELECT @var4 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Costs]') AND [c].[name] = N'Level4');
    IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [Costs] DROP CONSTRAINT [' + @var4 + '];');
    ALTER TABLE [Costs] ALTER COLUMN [Level4] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    DECLARE @var5 sysname;
    SELECT @var5 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Costs]') AND [c].[name] = N'Level3');
    IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [Costs] DROP CONSTRAINT [' + @var5 + '];');
    ALTER TABLE [Costs] ALTER COLUMN [Level3] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    DECLARE @var6 sysname;
    SELECT @var6 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Costs]') AND [c].[name] = N'Level2');
    IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Costs] DROP CONSTRAINT [' + @var6 + '];');
    ALTER TABLE [Costs] ALTER COLUMN [Level2] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    DECLARE @var7 sysname;
    SELECT @var7 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Costs]') AND [c].[name] = N'Level1');
    IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [Costs] DROP CONSTRAINT [' + @var7 + '];');
    ALTER TABLE [Costs] ALTER COLUMN [Level1] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327233718_SolveNamingInConceptsAndDepartment')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327233718_SolveNamingInConceptsAndDepartment', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210410193834_ChangeTheNameOfTheColumnToStringCustomersVendors')
BEGIN
    DECLARE @var8 sysname;
    SELECT @var8 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Vendors]') AND [c].[name] = N'Name');
    IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [Vendors] DROP CONSTRAINT [' + @var8 + '];');
    ALTER TABLE [Vendors] ALTER COLUMN [Name] nvarchar(250) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210410193834_ChangeTheNameOfTheColumnToStringCustomersVendors')
BEGIN
    DECLARE @var9 sysname;
    SELECT @var9 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Customers]') AND [c].[name] = N'Name');
    IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [Customers] DROP CONSTRAINT [' + @var9 + '];');
    ALTER TABLE [Customers] ALTER COLUMN [Name] nvarchar(250) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210410193834_ChangeTheNameOfTheColumnToStringCustomersVendors')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210410193834_ChangeTheNameOfTheColumnToStringCustomersVendors', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE TABLE [Sales] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [ProjectId] uniqueidentifier NOT NULL,
        [DepartmentId] uniqueidentifier NOT NULL,
        [PaymentMethodId] uniqueidentifier NOT NULL,
        [ContractDate] datetime2 NOT NULL,
        [CustomerId] uniqueidentifier NOT NULL,
        [Paid] nvarchar(50) NULL,
        [Residue] nvarchar(50) NULL,
        [Percentage] nvarchar(3) NULL,
        [AgentId] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        [ProjectsId] uniqueidentifier NULL,
        [DepartmentsId] uniqueidentifier NULL,
        [PaymentMethodsId] uniqueidentifier NULL,
        [CustomersId] uniqueidentifier NULL,
        [AgentsId] uniqueidentifier NULL,
        CONSTRAINT [PK_Sales] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Sales_Agents_AgentsId] FOREIGN KEY ([AgentsId]) REFERENCES [Agents] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_Customers_CustomersId] FOREIGN KEY ([CustomersId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_Departments_DepartmentsId] FOREIGN KEY ([DepartmentsId]) REFERENCES [Departments] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_PaymentMethods_PaymentMethodsId] FOREIGN KEY ([PaymentMethodsId]) REFERENCES [PaymentMethods] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Sales_Projects_ProjectsId] FOREIGN KEY ([ProjectsId]) REFERENCES [Projects] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_AgentsId] ON [Sales] ([AgentsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_CompaniesId] ON [Sales] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_CustomersId] ON [Sales] ([CustomersId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_DepartmentsId] ON [Sales] ([DepartmentsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_OfficesId] ON [Sales] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_PaymentMethodsId] ON [Sales] ([PaymentMethodsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    CREATE INDEX [IX_Sales_ProjectsId] ON [Sales] ([ProjectsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210522171645_AddSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210522171645_AddSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210601013700_AddConceptsDetailsTable')
BEGIN
    CREATE TABLE [ConceptsDetails] (
        [Id] uniqueidentifier NOT NULL,
        [Name] nvarchar(250) NULL,
        [ConceptId] uniqueidentifier NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [ConceptsId] uniqueidentifier NULL,
        CONSTRAINT [PK_ConceptsDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ConceptsDetails_Concepts_ConceptsId] FOREIGN KEY ([ConceptsId]) REFERENCES [Concepts] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210601013700_AddConceptsDetailsTable')
BEGIN
    CREATE INDEX [IX_ConceptsDetails_ConceptsId] ON [ConceptsDetails] ([ConceptsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210601013700_AddConceptsDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210601013700_AddConceptsDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210606204052_AddPercentajeToAgentsTable')
BEGIN
    ALTER TABLE [Agents] ADD [Percentaje] nvarchar(15) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210606204052_AddPercentajeToAgentsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210606204052_AddPercentajeToAgentsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210612190134_AddSalesDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210612190134_AddSalesDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613133225_AddSalesDetailsTableValues')
BEGIN
    CREATE TABLE [SalesDetails] (
        [Id] uniqueidentifier NOT NULL,
        [SaleId] uniqueidentifier NOT NULL,
        [PaidDate] datetime2 NOT NULL,
        [PaidImport] nvarchar(50) NULL,
        [ConceptId] uniqueidentifier NOT NULL,
        [StatusId] int NOT NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [SalesId] uniqueidentifier NULL,
        CONSTRAINT [PK_SalesDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SalesDetails_Sales_SalesId] FOREIGN KEY ([SalesId]) REFERENCES [Sales] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613133225_AddSalesDetailsTableValues')
BEGIN
    CREATE INDEX [IX_SalesDetails_SalesId] ON [SalesDetails] ([SalesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613133225_AddSalesDetailsTableValues')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210613133225_AddSalesDetailsTableValues', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613175529_ResolvePercentageLengthInSalesTable')
BEGIN
    DECLARE @var10 sysname;
    SELECT @var10 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sales]') AND [c].[name] = N'Percentage');
    IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [Sales] DROP CONSTRAINT [' + @var10 + '];');
    ALTER TABLE [Sales] ALTER COLUMN [Percentage] nvarchar(50) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613175529_ResolvePercentageLengthInSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210613175529_ResolvePercentageLengthInSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613212323_AddSchemeToSalesTable')
BEGIN
    ALTER TABLE [Sales] ADD [Scheme] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613212323_AddSchemeToSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210613212323_AddSchemeToSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613220954_AddMoreValuesInSalesTable')
BEGIN
    DECLARE @var11 sysname;
    SELECT @var11 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sales]') AND [c].[name] = N'PaymentMethodId');
    IF @var11 IS NOT NULL EXEC(N'ALTER TABLE [Sales] DROP CONSTRAINT [' + @var11 + '];');
    ALTER TABLE [Sales] DROP COLUMN [PaymentMethodId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613220954_AddMoreValuesInSalesTable')
BEGIN
    ALTER TABLE [Sales] ADD [ComisionAgent] nvarchar(50) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613220954_AddMoreValuesInSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210613220954_AddMoreValuesInSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613222024_AddSalePriceInSalesTable')
BEGIN
    ALTER TABLE [Sales] ADD [SalePrice] nvarchar(50) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210613222024_AddSalePriceInSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210613222024_AddSalePriceInSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210615033714_AddPercentajeAgentToSalesTable')
BEGIN
    ALTER TABLE [Sales] ADD [PercentageAgent] nvarchar(50) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210615033714_AddPercentajeAgentToSalesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210615033714_AddPercentajeAgentToSalesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210616003815_AddValueInSalesDetailsTable')
BEGIN
    ALTER TABLE [SalesDetails] ADD [Value] nvarchar(max) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210616003815_AddValueInSalesDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210616003815_AddValueInSalesDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210616004700_RemoveValueInSalesDetailsTable')
BEGIN
    DECLARE @var12 sysname;
    SELECT @var12 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[SalesDetails]') AND [c].[name] = N'Value');
    IF @var12 IS NOT NULL EXEC(N'ALTER TABLE [SalesDetails] DROP CONSTRAINT [' + @var12 + '];');
    ALTER TABLE [SalesDetails] DROP COLUMN [Value];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210616004700_RemoveValueInSalesDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210616004700_RemoveValueInSalesDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210716105044_AddnewPropertiesInSalesAndSalesDetailsTable')
BEGIN
    ALTER TABLE [SalesDetails] ADD [PaymentMethodId] uniqueidentifier NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210716105044_AddnewPropertiesInSalesAndSalesDetailsTable')
BEGIN
    ALTER TABLE [Sales] ADD [ProgramId] uniqueidentifier NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210716105044_AddnewPropertiesInSalesAndSalesDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210716105044_AddnewPropertiesInSalesAndSalesDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE TABLE [Expenses] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [ProjectId] uniqueidentifier NOT NULL,
        [TypeId] uniqueidentifier NOT NULL,
        [ExpenseDate] datetime2 NOT NULL,
        [VendorId] uniqueidentifier NOT NULL,
        [ConceptId] uniqueidentifier NOT NULL,
        [SubConceptId] uniqueidentifier NOT NULL,
        [Contract] nvarchar(150) NULL,
        [Advance] nvarchar(150) NULL,
        [AdvancePercentage] nvarchar(150) NULL,
        [IMSSPercentage] nvarchar(150) NULL,
        [Factor] nvarchar(150) NULL,
        [Siroc] nvarchar(150) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        [ProjectsId] uniqueidentifier NULL,
        [VendorsId] uniqueidentifier NULL,
        [ConceptsId] uniqueidentifier NULL,
        [ConceptsDetailsId] uniqueidentifier NULL,
        [AgentsId] uniqueidentifier NULL,
        CONSTRAINT [PK_Expenses] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Expenses_Agents_AgentsId] FOREIGN KEY ([AgentsId]) REFERENCES [Agents] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_Concepts_ConceptsId] FOREIGN KEY ([ConceptsId]) REFERENCES [Concepts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_ConceptsDetails_ConceptsDetailsId] FOREIGN KEY ([ConceptsDetailsId]) REFERENCES [ConceptsDetails] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_Projects_ProjectsId] FOREIGN KEY ([ProjectsId]) REFERENCES [Projects] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Expenses_Vendors_VendorsId] FOREIGN KEY ([VendorsId]) REFERENCES [Vendors] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE TABLE [ExpensesDetails] (
        [Id] uniqueidentifier NOT NULL,
        [ExpenseId] uniqueidentifier NOT NULL,
        [PaidDate] datetime2 NOT NULL,
        [ExpenseTypeId] uniqueidentifier NOT NULL,
        [PaidNumber] nvarchar(150) NULL,
        [PaidImport] nvarchar(150) NULL,
        [Price] nvarchar(150) NULL,
        [TotalPrice] nvarchar(150) NULL,
        [Unit] nvarchar(150) NULL,
        [Counter] nvarchar(150) NULL,
        [Estimated] nvarchar(150) NULL,
        [Retention] nvarchar(150) NULL,
        [Amortization] nvarchar(150) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [ExpensesId] uniqueidentifier NULL,
        CONSTRAINT [PK_ExpensesDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ExpensesDetails_Expenses_ExpensesId] FOREIGN KEY ([ExpensesId]) REFERENCES [Expenses] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_AgentsId] ON [Expenses] ([AgentsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_CompaniesId] ON [Expenses] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_ConceptsDetailsId] ON [Expenses] ([ConceptsDetailsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_ConceptsId] ON [Expenses] ([ConceptsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_OfficesId] ON [Expenses] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_ProjectsId] ON [Expenses] ([ProjectsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_Expenses_VendorsId] ON [Expenses] ([VendorsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    CREATE INDEX [IX_ExpensesDetails_ExpensesId] ON [ExpensesDetails] ([ExpensesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210803234655_AddExpenseAndExpenseDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210803234655_AddExpenseAndExpenseDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062319_RemoveTypeInExpenseAndExpensesDetails')
BEGIN
    DECLARE @var13 sysname;
    SELECT @var13 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ExpensesDetails]') AND [c].[name] = N'ExpenseTypeId');
    IF @var13 IS NOT NULL EXEC(N'ALTER TABLE [ExpensesDetails] DROP CONSTRAINT [' + @var13 + '];');
    ALTER TABLE [ExpensesDetails] DROP COLUMN [ExpenseTypeId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062319_RemoveTypeInExpenseAndExpensesDetails')
BEGIN
    DECLARE @var14 sysname;
    SELECT @var14 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Expenses]') AND [c].[name] = N'TypeId');
    IF @var14 IS NOT NULL EXEC(N'ALTER TABLE [Expenses] DROP CONSTRAINT [' + @var14 + '];');
    ALTER TABLE [Expenses] DROP COLUMN [TypeId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062319_RemoveTypeInExpenseAndExpensesDetails')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210804062319_RemoveTypeInExpenseAndExpensesDetails', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062633_AddTypeInExpenseAndExpensesDetails')
BEGIN
    ALTER TABLE [ExpensesDetails] ADD [ExpenseTypeId] int NOT NULL DEFAULT 0;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062633_AddTypeInExpenseAndExpensesDetails')
BEGIN
    ALTER TABLE [Expenses] ADD [TypeId] int NOT NULL DEFAULT 0;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210804062633_AddTypeInExpenseAndExpensesDetails')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210804062633_AddTypeInExpenseAndExpensesDetails', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE TABLE [Budgets] (
        [Id] uniqueidentifier NOT NULL,
        [CompanyId] uniqueidentifier NOT NULL,
        [OfficeId] uniqueidentifier NOT NULL,
        [ProjectId] uniqueidentifier NOT NULL,
        [TypeId] int NOT NULL,
        [Projection] nvarchar(150) NULL,
        [Executed] nvarchar(150) NULL,
        [Advance] nvarchar(150) NULL,
        [Total] nvarchar(150) NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [CompaniesId] uniqueidentifier NULL,
        [OfficesId] uniqueidentifier NULL,
        [ProjectsId] uniqueidentifier NULL,
        [ProgramsId] uniqueidentifier NULL,
        [ConceptsId] uniqueidentifier NULL,
        CONSTRAINT [PK_Budgets] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Budgets_Companies_CompaniesId] FOREIGN KEY ([CompaniesId]) REFERENCES [Companies] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Budgets_Concepts_ConceptsId] FOREIGN KEY ([ConceptsId]) REFERENCES [Concepts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Budgets_Offices_OfficesId] FOREIGN KEY ([OfficesId]) REFERENCES [Offices] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Budgets_Programs_ProgramsId] FOREIGN KEY ([ProgramsId]) REFERENCES [Programs] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Budgets_Projects_ProjectsId] FOREIGN KEY ([ProjectsId]) REFERENCES [Projects] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE TABLE [BudgetsDetails] (
        [Id] uniqueidentifier NOT NULL,
        [BudgetId] uniqueidentifier NOT NULL,
        [YearId] int NOT NULL,
        [MonthId] int NOT NULL,
        [ProgramId] uniqueidentifier NULL,
        [ConceptId] uniqueidentifier NULL,
        [CreatorUser] nvarchar(150) NULL,
        [CreationDate] datetime2 NOT NULL,
        [ModifUser] nvarchar(150) NULL,
        [ModifDate] datetime2 NULL,
        [BudgetsId] uniqueidentifier NULL,
        CONSTRAINT [PK_BudgetsDetails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BudgetsDetails_Budgets_BudgetsId] FOREIGN KEY ([BudgetsId]) REFERENCES [Budgets] ([Id]) ON DELETE NO ACTION
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_Budgets_CompaniesId] ON [Budgets] ([CompaniesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_Budgets_ConceptsId] ON [Budgets] ([ConceptsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_Budgets_OfficesId] ON [Budgets] ([OfficesId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_Budgets_ProgramsId] ON [Budgets] ([ProgramsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_Budgets_ProjectsId] ON [Budgets] ([ProjectsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    CREATE INDEX [IX_BudgetsDetails_BudgetsId] ON [BudgetsDetails] ([BudgetsId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210819045619_CreateBudgetsAndBudgetsDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210819045619_CreateBudgetsAndBudgetsDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    ALTER TABLE [Budgets] DROP CONSTRAINT [FK_Budgets_Concepts_ConceptsId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    ALTER TABLE [Budgets] DROP CONSTRAINT [FK_Budgets_Programs_ProgramsId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    DROP INDEX [IX_Budgets_ConceptsId] ON [Budgets];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    DROP INDEX [IX_Budgets_ProgramsId] ON [Budgets];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    DECLARE @var15 sysname;
    SELECT @var15 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Budgets]') AND [c].[name] = N'ConceptsId');
    IF @var15 IS NOT NULL EXEC(N'ALTER TABLE [Budgets] DROP CONSTRAINT [' + @var15 + '];');
    ALTER TABLE [Budgets] DROP COLUMN [ConceptsId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    DECLARE @var16 sysname;
    SELECT @var16 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Budgets]') AND [c].[name] = N'ProgramsId');
    IF @var16 IS NOT NULL EXEC(N'ALTER TABLE [Budgets] DROP CONSTRAINT [' + @var16 + '];');
    ALTER TABLE [Budgets] DROP COLUMN [ProgramsId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    DECLARE @var17 sysname;
    SELECT @var17 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Budgets]') AND [c].[name] = N'Total');
    IF @var17 IS NOT NULL EXEC(N'ALTER TABLE [Budgets] DROP CONSTRAINT [' + @var17 + '];');
    ALTER TABLE [Budgets] DROP COLUMN [Total];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    ALTER TABLE [BudgetsDetails] ADD [Total] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210911063305_RemoveBudgetsDetailsFromBudgetsTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211017232314_AddSuperUserColumnInProfilesTable')
BEGIN
    DECLARE @var18 sysname;
    SELECT @var18 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Profiles]') AND [c].[name] = N'Name');
    IF @var18 IS NOT NULL EXEC(N'ALTER TABLE [Profiles] DROP CONSTRAINT [' + @var18 + '];');
    ALTER TABLE [Profiles] ALTER COLUMN [Name] nvarchar(150) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211017232314_AddSuperUserColumnInProfilesTable')
BEGIN
    ALTER TABLE [Profiles] ADD [SuperUser] bit NOT NULL DEFAULT CAST(0 AS bit);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211017232314_AddSuperUserColumnInProfilesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20211017232314_AddSuperUserColumnInProfilesTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211018061804_ChangesInUserAndUsersDetailTable')
BEGIN
    DECLARE @var19 sysname;
    SELECT @var19 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'OfficeId');
    IF @var19 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var19 + '];');
    ALTER TABLE [Users] DROP COLUMN [OfficeId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211018061804_ChangesInUserAndUsersDetailTable')
BEGIN
    ALTER TABLE [UsersDetails] ADD [RoleId] uniqueidentifier NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211018061804_ChangesInUserAndUsersDetailTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20211018061804_ChangesInUserAndUsersDetailTable', N'5.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211018100907_ChangeProfileIdNameInUserDetailsTable')
BEGIN
    EXEC sp_rename N'[UsersDetails].[RoleId]', N'ProfileId', N'COLUMN';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211018100907_ChangeProfileIdNameInUserDetailsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20211018100907_ChangeProfileIdNameInUserDetailsTable', N'5.0.3');
END;
GO

COMMIT;
GO

