﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json.Serialization;

namespace Web.Contracts
{
    public class StandardResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }

        [JsonIgnore]
        public IActionResult Result
        {
            get
            {
                return new ObjectResult(this)
                {
                    StatusCode = (int)StatusCode
                };
            }
        }

        public StandardResponse(HttpStatusCode statusCode, T data = default, string message = "")
        {
            Data = data;
            StatusCode = statusCode;
            Message = message;
        }
    }
}
