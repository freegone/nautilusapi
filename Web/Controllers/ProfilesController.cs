﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class ProfilesController : ControllerBase
    {
        private readonly IProfileService _profileService;

        public ProfilesController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [HttpGet(nameof(GetProfiles))]
        public IActionResult GetProfiles()
        {
            var result = _profileService.GetProfiles();
            return new StandardResponse<List<ProfileResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetProfile))]
        public IActionResult GetProfile(Guid id)
        {
            var result = _profileService.GetProfile(id);
            return new StandardResponse<ProfileResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetProfileByUserId))]
        public IActionResult GetProfileByUserId(Guid id)
        {
            var result = _profileService.GetProfileByUserId(id);
            return new StandardResponse<ProfileResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveProfile))]
        public async Task<IActionResult> SaveProfile(ProfileRequest request)
        {
            var result = await _profileService.SaveProfile(request);
            return new StandardResponse<ProfileResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateProfile))]
        public async Task<IActionResult> UpdateProfile(ProfileRequest request)
        {
            var result = await _profileService.UpdateProfile(request);
            return new StandardResponse<ProfileResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteProfile))]
        public async Task<IActionResult> DeleteProfile(Guid id)
        {
            var result = await _profileService.DeleteProfile(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}