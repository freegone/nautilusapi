﻿using Global.Requests;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomersService _customersService;

        public CustomersController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        [HttpGet(nameof(GetCustomers))]
        public IActionResult GetCustomers()
        {
            var result = _customersService.GetCustomers();
            return new StandardResponse<List<CustomerResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetCustomer))]
        public IActionResult GetCustomer(Guid id)
        {
            var result = _customersService.GetCustomer(id);
            return new StandardResponse<CustomerResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveCustomer))]
        public async Task<IActionResult> SaveCustomer(CustomerRequest request)
        {
            var result = await _customersService.SaveCustomer(request);
            return new StandardResponse<CustomerResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateCustomer))]
        public async Task<IActionResult> UpdateCustomer(CustomerRequest request)
        {
            var result = await _customersService.UpdateCustomer(request);
            return new StandardResponse<CustomerResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteCustomer))]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var result = await _customersService.DeleteCustomer(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}