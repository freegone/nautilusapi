﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class SalesController : ControllerBase
    {
        private readonly ISalesService _salesService;

        public SalesController(ISalesService salesService)
        {
            _salesService = salesService;
        }

        [HttpGet(nameof(GetSales))]
        public IActionResult GetSales()
        {
            var result = _salesService.GetSales();
            return new StandardResponse<List<SaleResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetSale))]
        public IActionResult GetSale(Guid id)
        {
            var result = _salesService.GetSale(id);
            return new StandardResponse<SaleResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveSale))]
        public async Task<IActionResult> SaveSale(SaleRequest request)
        {
            var result = await _salesService.SaveSale(request);
            return new StandardResponse<SaleResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateSale))]
        public async Task<IActionResult> UpdateSale(SaleRequest request)
        {
            var result = await _salesService.UpdateSale(request);
            return new StandardResponse<SaleResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteSale))]
        public async Task<IActionResult> DeleteSale(Guid id)
        {
            var result = await _salesService.DeleteSale(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}
