﻿using Global.Requests;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class VendorsController : ControllerBase
    {
        private readonly IVendorsService _vendorsService;

        public VendorsController(IVendorsService vendorsService)
        {
            _vendorsService = vendorsService;
        }

        [HttpGet(nameof(GetVendors))]
        public IActionResult GetVendors()
        {
            var result = _vendorsService.GetVendors();
            return new StandardResponse<List<VendorResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetVendor))]
        public IActionResult GetVendor(Guid id)
        {
            var result = _vendorsService.GetVendor(id);
            return new StandardResponse<VendorResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveVendor))]
        public async Task<IActionResult> SaveVendor(VendorRequest request)
        {
            var result = await _vendorsService.SaveVendor(request);
            return new StandardResponse<VendorResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateVendor))]
        public async Task<IActionResult> UpdateVendor(VendorRequest request)
        {
            var result = await _vendorsService.UpdateVendor(request);
            return new StandardResponse<VendorResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteVendor))]
        public async Task<IActionResult> DeleteVendor(Guid id)
        {
            var result = await _vendorsService.DeleteVendor(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}