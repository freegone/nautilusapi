﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class AgentsController : ControllerBase
    {
        private readonly IAgentsService _agentsService;

        public AgentsController(IAgentsService agentsService)
        {
            _agentsService = agentsService;
        }

        [HttpGet(nameof(GetAgents))]
        public IActionResult GetAgents()
        {
            var result = _agentsService.GetAgents();
            return new StandardResponse<List<AgentResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetAgent))]
        public IActionResult GetAgent(Guid id)
        {
            var result = _agentsService.GetAgent(id);
            return new StandardResponse<AgentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveAgent))]
        public async Task<IActionResult> SaveAgent(AgentRequest request)
        {
            var result = await _agentsService.SaveAgent(request);
            return new StandardResponse<AgentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateAgent))]
        public async Task<IActionResult> UpdateAgent(AgentRequest request)
        {
            var result = await _agentsService.UpdateAgent(request);
            return new StandardResponse<AgentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteAgent))]
        public async Task<IActionResult> DeleteAgent(Guid id)
        {
            var result = await _agentsService.DeleteAgent(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}