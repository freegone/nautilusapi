﻿using Global.Responses;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Collections.Generic;
using System.Net;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class HomeController : ControllerBase
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        [HttpGet(nameof(GetOverview))]
        public IActionResult GetOverview()
        {
            var result = _homeService.GetOverview();
            return new StandardResponse<OverviewResponse>(HttpStatusCode.OK, result).Result;
        }
    }
}
