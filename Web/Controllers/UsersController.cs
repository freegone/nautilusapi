﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet(nameof(GetUsers))]
        public IActionResult GetUsers()
        {
            var result = _userService.GetUsers();
            return new StandardResponse<List<UserResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetUser))]
        public IActionResult GetUser(Guid id)
        {
            var result = _userService.GetUser(id);
            return new StandardResponse<UserResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveUser))]
        public async Task<IActionResult> SaveUser(UserRequest request)
        {
            var result = await _userService.SaveUser(request);
            return new StandardResponse<UserResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateUser))]
        public async Task<IActionResult> UpdateUser(UserRequest request)
        {
            var result = await _userService.UpdateUser(request);
            return new StandardResponse<UserResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteUser))]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var result = await _userService.DeleteUser(id);
            return new StandardResponse<bool>(HttpStatusCode.OK, result).Result;
        }
    }
}