﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class PaymentMethodsController : ControllerBase
    {
        private readonly IPaymentMethodsService _paymentMethodsService;

        public PaymentMethodsController(IPaymentMethodsService paymentMethodsService)
        {
            _paymentMethodsService = paymentMethodsService;
        }

        [HttpGet(nameof(GetPaymentMethods))]
        public IActionResult GetPaymentMethods()
        {
            var result = _paymentMethodsService.GetPaymentMethods();
            return new StandardResponse<List<PaymentMethodResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetPaymentMethod))]
        public IActionResult GetPaymentMethod(Guid id)
        {
            var result = _paymentMethodsService.GetPaymentMethod(id);
            return new StandardResponse<PaymentMethodResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SavePaymentMethod))]
        public async Task<IActionResult> SavePaymentMethod(PaymentMethodRequest request)
        {
            var result = await _paymentMethodsService.SavePaymentMethod(request);
            return new StandardResponse<PaymentMethodResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdatePaymentMethod))]
        public async Task<IActionResult> UpdatePaymentMethod(PaymentMethodRequest request)
        {
            var result = await _paymentMethodsService.UpdatePaymentMethod(request);
            return new StandardResponse<PaymentMethodResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeletePaymentMethod))]
        public async Task<IActionResult> DeletePaymentMethod(Guid id)
        {
            var result = await _paymentMethodsService.DeletePaymentMethod(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}