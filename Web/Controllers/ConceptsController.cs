﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class ConceptsController : ControllerBase
    {
        private readonly IConceptsService _conceptsService;

        public ConceptsController(IConceptsService conceptsService)
        {
            _conceptsService = conceptsService;
        }

        [HttpGet(nameof(GetConcepts))]
        public IActionResult GetConcepts()
        {
            var result = _conceptsService.GetConcepts();
            return new StandardResponse<List<ConceptResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetConcept))]
        public IActionResult GetConcept(Guid id)
        {
            var result = _conceptsService.GetConcept(id);
            return new StandardResponse<ConceptResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveConcept))]
        public async Task<IActionResult> SaveConcept(ConceptRequest request)
        {
            var result = await _conceptsService.SaveConcept(request);
            return new StandardResponse<ConceptResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateConcept))]
        public async Task<IActionResult> UpdateConcept(ConceptRequest request)
        {
            var result = await _conceptsService.UpdateConcept(request);
            return new StandardResponse<ConceptResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteConcept))]
        public async Task<IActionResult> DeleteConcept(Guid id)
        {
            var result = await _conceptsService.DeleteConcept(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}