﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class CostsController : ControllerBase
    {
        private readonly ICostsService _costsService;

        public CostsController(ICostsService costsService)
        {
            _costsService = costsService;
        }

        [HttpGet(nameof(GetCosts))]
        public IActionResult GetCosts()
        {
            var result = _costsService.GetCosts();
            return new StandardResponse<List<CostResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetCost))]
        public IActionResult GetCost(Guid id)
        {
            var result = _costsService.GetCost(id);
            return new StandardResponse<CostResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveCost))]
        public async Task<IActionResult> SaveCost(CostRequest request)
        {
            var result = await _costsService.SaveCost(request);
            return new StandardResponse<CostResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateCost))]
        public async Task<IActionResult> UpdateCost(CostRequest request)
        {
            var result = await _costsService.UpdateCost(request);
            return new StandardResponse<CostResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteCost))]
        public async Task<IActionResult> DeleteCost(Guid id)
        {
            var result = await _costsService.DeleteCost(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}