﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet("GetProjects/{allDepartments}")]
        public IActionResult GetProjects(bool allDepartments = true)
        {
            var result = _projectsService.GetProjects(allDepartments);
            return new StandardResponse<List<ProjectResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetProject))]
        public IActionResult GetProject(Guid id)
        {
            var result = _projectsService.GetProject(id);
            return new StandardResponse<ProjectResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveProject))]
        public async Task<IActionResult> SaveProject(ProjectRequest request)
        {
            var result = await _projectsService.SaveProject(request);
            return new StandardResponse<ProjectResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateProject))]
        public async Task<IActionResult> UpdateProject(ProjectRequest request)
        {
            var result = await _projectsService.UpdateProject(request);
            return new StandardResponse<ProjectResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteProject))]
        public async Task<IActionResult> DeleteProject(Guid id)
        {
            var result = await _projectsService.DeleteProject(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}