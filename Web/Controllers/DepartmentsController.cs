﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDeparmentsService _deparmentsService;

        public DepartmentsController(IDeparmentsService deparmentsService)
        {
            _deparmentsService = deparmentsService;
        }

        [HttpGet(nameof(GetDepartments))]
        public IActionResult GetDepartments()
        {
            var result = _deparmentsService.GetDepartments();
            return new StandardResponse<List<DepartmentResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetDepartment))]
        public IActionResult GetDepartment(Guid id)
        {
            var result = _deparmentsService.GetDepartment(id);
            return new StandardResponse<DepartmentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveDepartment))]
        public async Task<IActionResult> SaveDepartment(DepartmentRequest request)
        {
            var result = await _deparmentsService.SaveDepartment(request);
            return new StandardResponse<DepartmentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateDepartment))]
        public async Task<IActionResult> UpdateDepartment(DepartmentRequest request)
        {
            var result = await _deparmentsService.UpdateDepartment(request);
            return new StandardResponse<DepartmentResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteDepartment))]
        public async Task<IActionResult> DeleteDepartment(Guid id)
        {
            var result = await _deparmentsService.DeleteDepartment(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}