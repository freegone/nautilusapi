﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class ProgramsController : ControllerBase
    {
        private readonly IProgramsService _programsService;

        public ProgramsController(IProgramsService programsService)
        {
            _programsService = programsService;
        }

        [HttpGet(nameof(GetPrograms))]
        public IActionResult GetPrograms()
        {
            var result = _programsService.GetPrograms();
            return new StandardResponse<List<ProgramResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetProgram))]
        public IActionResult GetProgram(Guid id)
        {
            var result = _programsService.GetProgram(id);
            return new StandardResponse<ProgramResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveProgram))]
        public async Task<IActionResult> SaveProgram(ProgramRequest request)
        {
            var result = await _programsService.SaveProgram(request);
            return new StandardResponse<ProgramResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateProgram))]
        public async Task<IActionResult> UpdateProgram(ProgramRequest request)
        {
            var result = await _programsService.UpdateProgram(request);
            return new StandardResponse<ProgramResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteProgram))]
        public async Task<IActionResult> DeleteProgram(Guid id)
        {
            var result = await _programsService.DeleteProgram(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}