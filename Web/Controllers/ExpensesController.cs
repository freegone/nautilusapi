﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class ExpensesController : ControllerBase
    {
        private readonly IExpensesService _expensesService;

        public ExpensesController(IExpensesService expensesService)
        {
            _expensesService = expensesService;
        }

        [HttpGet(nameof(GetExpenses))]
        public IActionResult GetExpenses()
        {
            var result = _expensesService.GetExpenses();
            return new StandardResponse<List<ExpenseResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetExpense))]
        public IActionResult GetExpense(Guid id)
        {
            var result = _expensesService.GetExpense(id);
            return new StandardResponse<ExpenseResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveExpense))]
        public async Task<IActionResult> SaveExpense(ExpenseRequest request)
        {
            var result = await _expensesService.SaveExpense(request);
            return new StandardResponse<ExpenseResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateExpense))]
        public async Task<IActionResult> UpdateExpense(ExpenseRequest request)
        {
            var result = await _expensesService.UpdateExpense(request);
            return new StandardResponse<ExpenseResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteExpense))]
        public async Task<IActionResult> DeleteExpense(Guid id)
        {
            var result = await _expensesService.DeleteExpense(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}
