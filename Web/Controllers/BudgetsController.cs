﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class BudgetsController : ControllerBase
    {
        private readonly IBudgetsService _budgetsService;

        public BudgetsController(IBudgetsService budgetsService)
        {
            _budgetsService = budgetsService;
        }

        [HttpGet(nameof(GetBudgets))]
        public IActionResult GetBudgets()
        {
            var result = _budgetsService.GetBudgets();
            return new StandardResponse<List<BudgetResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetBudget))]
        public IActionResult GetBudget(Guid id)
        {
            var result = _budgetsService.GetBudget(id);
            return new StandardResponse<BudgetResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveBudget))]
        public async Task<IActionResult> SaveBudget(BudgetRequest request)
        {
            var result = await _budgetsService.SaveBudget(request);
            return new StandardResponse<BudgetResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateBudget))]
        public async Task<IActionResult> UpdateBudget(BudgetRequest request)
        {
            var result = await _budgetsService.UpdateBudget(request);
            return new StandardResponse<BudgetResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteBudget))]
        public async Task<IActionResult> DeleteBudget(Guid id)
        {
            var result = await _budgetsService.DeleteBudget(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}
