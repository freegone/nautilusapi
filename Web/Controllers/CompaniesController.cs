﻿using Global.Requests;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompaniesService _companiesService;

        public CompaniesController(ICompaniesService companiesService)
        {
            _companiesService = companiesService;
        }

        [HttpGet(nameof(GetCompanies))]
        public IActionResult GetCompanies()
        {
            var result = _companiesService.GetCompanies();
            return new StandardResponse<List<CompanyResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetCompany))]
        public IActionResult GetCompany(Guid id)
        {
            var result = _companiesService.GetCompany(id);
            return new StandardResponse<CompanyResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveCompany))]
        public async Task<IActionResult> SaveCompany(CompanyRequest request)
        {
            var result = await _companiesService.SaveCompany(request);
            return new StandardResponse<CompanyResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateCompany))]
        public async Task<IActionResult> UpdateCompany(CompanyRequest request)
        {
            var result = await _companiesService.UpdateCompany(request);
            return new StandardResponse<CompanyResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteCompany))]
        public async Task<IActionResult> DeleteCompany(Guid id)
        {
            var result = await _companiesService.DeleteCompany(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}