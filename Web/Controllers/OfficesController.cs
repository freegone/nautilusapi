﻿using Global.Requests;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class OfficesController : ControllerBase
    {
        private readonly IOfficesServices _officesServices;

        public OfficesController(IOfficesServices officesServices)
        {
            _officesServices = officesServices;
        }

        [HttpGet(nameof(GetOffices))]
        public IActionResult GetOffices()
        {
            var result = _officesServices.GetOffices();
            return new StandardResponse<List<OfficeResponse>>(HttpStatusCode.OK, result).Result;
        }

        [HttpGet(nameof(GetOffice))]
        public IActionResult GetOffice(Guid id)
        {
            var result = _officesServices.GetOffice(id);
            return new StandardResponse<OfficeResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPost(nameof(SaveOffice))]
        public async Task<IActionResult> SaveOffice(OfficeRequest request)
        {
            var result = await _officesServices.SaveOffice(request);
            return new StandardResponse<OfficeResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpPut(nameof(UpdateOffice))]
        public async Task<IActionResult> UpdateOffice(OfficeRequest request)
        {
            var result = await _officesServices.UpdateOffice(request);
            return new StandardResponse<OfficeResponse>(HttpStatusCode.OK, result).Result;
        }

        [HttpDelete(nameof(DeleteOffice))]
        public async Task<IActionResult> DeleteOffice(Guid id)
        {
            var result = await _officesServices.DeleteOffice(id);
            return new StandardResponse<bool?>(HttpStatusCode.OK, result).Result;
        }
    }
}