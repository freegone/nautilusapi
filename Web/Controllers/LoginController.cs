﻿using Global;
using Global.Responses;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Net;
using Web.Contracts;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors("nautilusPolicy")]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost(nameof(DoLogin))]
        public IActionResult DoLogin(LoginRequest request)
        {
            var result = _loginService.DoLogin(request);
            return new StandardResponse<LoginResponse>(HttpStatusCode.OK, result).Result;
        }
    }
}