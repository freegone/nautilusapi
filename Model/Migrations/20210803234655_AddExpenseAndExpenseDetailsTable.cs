﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class AddExpenseAndExpenseDetailsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Expenses",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExpenseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    VendorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ConceptId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubConceptId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Contract = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Advance = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    AdvancePercentage = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    IMSSPercentage = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Factor = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Siroc = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProjectsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    VendorsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ConceptsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ConceptsDetailsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AgentsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expenses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Expenses_Agents_AgentsId",
                        column: x => x.AgentsId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_Concepts_ConceptsId",
                        column: x => x.ConceptsId,
                        principalTable: "Concepts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_ConceptsDetails_ConceptsDetailsId",
                        column: x => x.ConceptsDetailsId,
                        principalTable: "ConceptsDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_Projects_ProjectsId",
                        column: x => x.ProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_Vendors_VendorsId",
                        column: x => x.VendorsId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExpensesDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExpenseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaidDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpenseTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaidNumber = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    PaidImport = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Price = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    TotalPrice = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Unit = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Counter = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Estimated = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Retention = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Amortization = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ExpensesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpensesDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExpensesDetails_Expenses_ExpensesId",
                        column: x => x.ExpensesId,
                        principalTable: "Expenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_AgentsId",
                table: "Expenses",
                column: "AgentsId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_CompaniesId",
                table: "Expenses",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ConceptsDetailsId",
                table: "Expenses",
                column: "ConceptsDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ConceptsId",
                table: "Expenses",
                column: "ConceptsId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_OfficesId",
                table: "Expenses",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ProjectsId",
                table: "Expenses",
                column: "ProjectsId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_VendorsId",
                table: "Expenses",
                column: "VendorsId");

            migrationBuilder.CreateIndex(
                name: "IX_ExpensesDetails_ExpensesId",
                table: "ExpensesDetails",
                column: "ExpensesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExpensesDetails");

            migrationBuilder.DropTable(
                name: "Expenses");
        }
    }
}
