﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class RemoveBudgetsDetailsFromBudgetsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_Concepts_ConceptsId",
                table: "Budgets");

            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_Programs_ProgramsId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_ConceptsId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_ProgramsId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "ConceptsId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "ProgramsId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Budgets");

            migrationBuilder.AddColumn<string>(
                name: "Total",
                table: "BudgetsDetails",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Total",
                table: "BudgetsDetails");

            migrationBuilder.AddColumn<Guid>(
                name: "ConceptsId",
                table: "Budgets",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProgramsId",
                table: "Budgets",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Total",
                table: "Budgets",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ConceptsId",
                table: "Budgets",
                column: "ConceptsId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ProgramsId",
                table: "Budgets",
                column: "ProgramsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_Concepts_ConceptsId",
                table: "Budgets",
                column: "ConceptsId",
                principalTable: "Concepts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_Programs_ProgramsId",
                table: "Budgets",
                column: "ProgramsId",
                principalTable: "Programs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
