﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Access",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(35)", maxLength: 35, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Access", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    RFC = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Modules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Offices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Offices_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProfilesDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AccessId = table.Column<int>(type: "int", nullable: false),
                    ProfileId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ModuleId = table.Column<int>(type: "int", nullable: false),
                    ActionView = table.Column<bool>(type: "bit", nullable: false),
                    ActionAdd = table.Column<bool>(type: "bit", nullable: false),
                    ActionEdit = table.Column<bool>(type: "bit", nullable: false),
                    ActionDelete = table.Column<bool>(type: "bit", nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ProfilesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModulesId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfilesDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfilesDetails_Access_AccessId",
                        column: x => x.AccessId,
                        principalTable: "Access",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfilesDetails_Modules_ModulesId",
                        column: x => x.ModulesId,
                        principalTable: "Modules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProfilesDetails_Profiles_ProfilesId",
                        column: x => x.ProfilesId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Agents",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<Guid>(type: "uniqueidentifier", maxLength: 250, nullable: false),
                    RFC = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Agents_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Agents_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Concepts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Concepts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Concepts_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Concepts_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Costs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Level1 = table.Column<Guid>(type: "uniqueidentifier", maxLength: 150, nullable: false),
                    Level2 = table.Column<Guid>(type: "uniqueidentifier", maxLength: 150, nullable: false),
                    Level3 = table.Column<Guid>(type: "uniqueidentifier", maxLength: 150, nullable: false),
                    Level4 = table.Column<Guid>(type: "uniqueidentifier", maxLength: 150, nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Costs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Costs_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Costs_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<Guid>(type: "uniqueidentifier", maxLength: 250, nullable: false),
                    RFC = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<Guid>(type: "uniqueidentifier", maxLength: 250, nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentMethods_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentMethods_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Programs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<Guid>(type: "uniqueidentifier", maxLength: 250, nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Programs_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Programs_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Units = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProfileId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ProfilesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Profiles_ProfilesId",
                        column: x => x.ProfilesId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vendors",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<Guid>(type: "uniqueidentifier", maxLength: 250, nullable: false),
                    RFC = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vendors_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vendors_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Unit = table.Column<Guid>(type: "uniqueidentifier", maxLength: 150, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    SalePrice = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Characteristics = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProjectsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Departments_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Departments_Projects_ProjectsId",
                        column: x => x.ProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UsersDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UsersId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsersDetails_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UsersDetails_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Agents_CompaniesId",
                table: "Agents",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Agents_OfficesId",
                table: "Agents",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Concepts_CompaniesId",
                table: "Concepts",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Concepts_OfficesId",
                table: "Concepts",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Costs_CompaniesId",
                table: "Costs",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Costs_OfficesId",
                table: "Costs",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CompaniesId",
                table: "Customers",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_OfficesId",
                table: "Customers",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_CompaniesId",
                table: "Departments",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_OfficesId",
                table: "Departments",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_ProjectsId",
                table: "Departments",
                column: "ProjectsId");

            migrationBuilder.CreateIndex(
                name: "IX_Offices_CompaniesId",
                table: "Offices",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMethods_CompaniesId",
                table: "PaymentMethods",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMethods_OfficesId",
                table: "PaymentMethods",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfilesDetails_AccessId",
                table: "ProfilesDetails",
                column: "AccessId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfilesDetails_ModulesId",
                table: "ProfilesDetails",
                column: "ModulesId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfilesDetails_ProfilesId",
                table: "ProfilesDetails",
                column: "ProfilesId");

            migrationBuilder.CreateIndex(
                name: "IX_Programs_CompaniesId",
                table: "Programs",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Programs_OfficesId",
                table: "Programs",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CompaniesId",
                table: "Projects",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_OfficesId",
                table: "Projects",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompaniesId",
                table: "Users",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_OfficesId",
                table: "Users",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ProfilesId",
                table: "Users",
                column: "ProfilesId");

            migrationBuilder.CreateIndex(
                name: "IX_UsersDetails_OfficesId",
                table: "UsersDetails",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_UsersDetails_UsersId",
                table: "UsersDetails",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_CompaniesId",
                table: "Vendors",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_OfficesId",
                table: "Vendors",
                column: "OfficesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agents");

            migrationBuilder.DropTable(
                name: "Concepts");

            migrationBuilder.DropTable(
                name: "Costs");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "PaymentMethods");

            migrationBuilder.DropTable(
                name: "ProfilesDetails");

            migrationBuilder.DropTable(
                name: "Programs");

            migrationBuilder.DropTable(
                name: "UsersDetails");

            migrationBuilder.DropTable(
                name: "Vendors");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Access");

            migrationBuilder.DropTable(
                name: "Modules");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Offices");

            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
