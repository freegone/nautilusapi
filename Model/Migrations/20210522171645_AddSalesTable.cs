﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class AddSalesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DepartmentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaymentMethodId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ContractDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Paid = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Residue = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Percentage = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
                    AgentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProjectsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DepartmentsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PaymentMethodsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CustomersId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AgentsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sales_Agents_AgentsId",
                        column: x => x.AgentsId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Customers_CustomersId",
                        column: x => x.CustomersId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Departments_DepartmentsId",
                        column: x => x.DepartmentsId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_PaymentMethods_PaymentMethodsId",
                        column: x => x.PaymentMethodsId,
                        principalTable: "PaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Projects_ProjectsId",
                        column: x => x.ProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_AgentsId",
                table: "Sales",
                column: "AgentsId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_CompaniesId",
                table: "Sales",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_CustomersId",
                table: "Sales",
                column: "CustomersId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_DepartmentsId",
                table: "Sales",
                column: "DepartmentsId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_OfficesId",
                table: "Sales",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_PaymentMethodsId",
                table: "Sales",
                column: "PaymentMethodsId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_ProjectsId",
                table: "Sales",
                column: "ProjectsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sales");
        }
    }
}
