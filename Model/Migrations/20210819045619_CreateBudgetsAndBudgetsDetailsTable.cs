﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class CreateBudgetsAndBudgetsDetailsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Budgets",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    Projection = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Executed = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Advance = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Total = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompaniesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProjectsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProgramsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ConceptsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Budgets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Budgets_Companies_CompaniesId",
                        column: x => x.CompaniesId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budgets_Concepts_ConceptsId",
                        column: x => x.ConceptsId,
                        principalTable: "Concepts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budgets_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budgets_Programs_ProgramsId",
                        column: x => x.ProgramsId,
                        principalTable: "Programs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budgets_Projects_ProjectsId",
                        column: x => x.ProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BudgetsDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BudgetId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    YearId = table.Column<int>(type: "int", nullable: false),
                    MonthId = table.Column<int>(type: "int", nullable: false),
                    ProgramId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ConceptId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatorUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifUser = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ModifDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BudgetsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetsDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BudgetsDetails_Budgets_BudgetsId",
                        column: x => x.BudgetsId,
                        principalTable: "Budgets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_CompaniesId",
                table: "Budgets",
                column: "CompaniesId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ConceptsId",
                table: "Budgets",
                column: "ConceptsId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_OfficesId",
                table: "Budgets",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ProgramsId",
                table: "Budgets",
                column: "ProgramsId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ProjectsId",
                table: "Budgets",
                column: "ProjectsId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetsDetails_BudgetsId",
                table: "BudgetsDetails",
                column: "BudgetsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BudgetsDetails");

            migrationBuilder.DropTable(
                name: "Budgets");
        }
    }
}
