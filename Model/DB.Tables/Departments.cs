﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Departments : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        [MaxLength(150)]
        public string Unit { get; set; }

        [MaxLength(150)]
        public string Type { get; set; }

        [MaxLength(250)]
        public string Description { get; set; }

        [MaxLength(150)]
        public string SalePrice { get; set; }

        [MaxLength(150)]
        public string Characteristics { get; set; }

        public bool Status { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        public Projects Projects { get; set; }

        #endregion
    }
}
