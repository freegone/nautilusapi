﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class ExpensesDetails : EntityBase, ICreationDate
    {
        public Guid ExpenseId { get; set; }

        public DateTime PaidDate { get; set; }

        public int ExpenseTypeId { get; set; }

        [MaxLength(150)]
        public string PaidNumber { get; set; }

        [MaxLength(150)]
        public string PaidImport { get; set; }

        [MaxLength(150)]
        public string Price { get; set; }

        [MaxLength(150)]
        public string TotalPrice { get; set; }

        [MaxLength(150)]
        public string Unit { get; set; }

        [MaxLength(150)]
        public string Counter { get; set; }

        [MaxLength(150)]
        public string Estimated { get; set; }

        [MaxLength(150)]
        public string Retention { get; set; }

        [MaxLength(150)]
        public string Amortization { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Expenses Expenses { get; set; }
        #endregion
    }
}
