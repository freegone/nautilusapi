﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Users : EntityBase, ICreationDate
    {
        public Guid ProfileId { get; set; }

        public Guid CompanyId { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Email { get; set; }

        [MaxLength(15)]
        public string Phone { get; set; }

        [MaxLength(15)]
        public string Password { get; set; }

        public bool Status { get; set; }

        [MaxLength(50)]
        public string Picture { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Profiles Profiles { get; set; }

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        #endregion
    }
}
