﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Expenses : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public int TypeId { get; set; }

        public DateTime ExpenseDate { get; set; }

        public Guid VendorId { get; set; }

        public Guid ConceptId { get; set; }

        public Guid SubConceptId { get; set; }

        [MaxLength(150)]
        public string Contract { get; set; }

        [MaxLength(150)]
        public string Advance { get; set; }

        [MaxLength(150)]
        public string AdvancePercentage { get; set; }

        [MaxLength(150)]
        public string IMSSPercentage { get; set; }

        [MaxLength(150)]
        public string Factor { get; set; }

        [MaxLength(150)]
        public string Siroc { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        public Projects Projects { get; set; }

        public Vendors Vendors { get; set; }

        public Concepts Concepts { get; set; }

        public ConceptsDetails ConceptsDetails { get; set; }

        public Agents Agents { get; set; }

        #endregion
    }
}
