﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Vendors : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string RFC { get; set; }

        [MaxLength(150)]
        public string Email { get; set; }

        [MaxLength(15)]
        public string Phone { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }

        [MaxLength(300)]
        public string Notes { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        #endregion
    }
}
