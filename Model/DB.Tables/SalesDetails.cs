﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class SalesDetails : EntityBase, ICreationDate
    {
        public Guid SaleId { get; set; }

        public Guid PaymentMethodId { get; set; }

        public DateTime PaidDate { get; set; }

        [MaxLength(50)]
        public string PaidImport { get; set; }

        public Guid ConceptId { get; set; }

        public int StatusId { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Sales Sales { get; set; }
        #endregion
    }
}
