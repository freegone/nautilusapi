﻿using System;

namespace Model.DB.Tables.Interfaces
{
    public interface ICreationDate
    {
        DateTime CreationDate { get; set; }
    }
}
