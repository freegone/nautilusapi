﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class UsersDetails: EntityBase, ICreationDate
    {
        public Guid ProfileId { get; set; }

        public Guid OfficeId { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Users Users { get; set; }

        public Offices Offices { get; set; }

        #endregion
    }
}
