﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Sales : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid DepartmentId { get; set; }

        public Guid ProgramId { get; set; }

        public DateTime ContractDate { get; set; }

        public Guid CustomerId { get; set; }

        [MaxLength(150)]
        public string Scheme { get; set; }

        [MaxLength(50)]
        public string SalePrice { get; set; }

        [MaxLength(50)]
        public string Paid { get; set; }

        [MaxLength(50)]
        public string Residue { get; set; }

        [MaxLength(50)]
        public string Percentage { get; set; }

        public Guid AgentId { get; set; }

        [MaxLength(50)]
        public string PercentageAgent { get; set; }

        [MaxLength(50)]
        public string ComisionAgent { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        public Projects Projects { get; set; }

        public Departments Departments { get; set; }

        public PaymentMethods PaymentMethods { get; set; }

        public Customers Customers { get; set; }

        public Agents Agents { get; set; }

        #endregion
    }
}
