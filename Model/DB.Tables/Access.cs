﻿using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Access
    {
        public int Id { get; set; }

        [MaxLength(35)]
        public string Name { get; set; }
    }
}
