﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Costs : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        [MaxLength(150)]
        public string Level1 { get; set; }

        [MaxLength(150)]
        public string Level2 { get; set; }

        [MaxLength(150)]
        public string Level3 { get; set; }

        [MaxLength(150)]
        public string Level4 { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        #endregion
    }
}
