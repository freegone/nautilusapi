﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Budgets : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public int TypeId { get; set; }

        [MaxLength(150)]
        public string Projection { get; set; }

        [MaxLength(150)]
        public string Executed { get; set; }

        [MaxLength(150)]
        public string Advance { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        public Projects Projects { get; set; }

        #endregion
    }
}
