﻿using Model.DB.Tables;
using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Projects : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Units { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        public Offices Offices { get; set; }

        #endregion
    }
}
