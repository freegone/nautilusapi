﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class Offices : EntityBase, ICreationDate
    {
        public Guid CompanyId { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }

        [MaxLength(300)]
        public string Notes { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Companies Companies { get; set; }

        #endregion
    }
}
