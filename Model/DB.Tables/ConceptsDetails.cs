﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class ConceptsDetails : EntityBase, ICreationDate
    {
        [MaxLength(250)]
        public string Name { get; set; }

        public Guid ConceptId { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Concepts Concepts { get; set; }

        #endregion
    }
}
