﻿using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    // Settings
    public class Modules
    {
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }
    }
}
