﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class BudgetsDetails : EntityBase, ICreationDate
    {
        public Guid BudgetId { get; set; }

        public int YearId { get; set; }

        public int MonthId { get; set; }

        public Guid? ProgramId { get; set; }

        public Guid? ConceptId { get; set; }

        [MaxLength(150)]
        public string Total { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Budgets Budgets { get; set; }
        #endregion
    }
}
