﻿using Model.DB.Tables.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Model.DB.Tables
{
    public class ProfilesDetails : EntityBase, ICreationDate
    {
        public int AccessId { get; set; }

        public Guid ProfileId { get; set; }

        public int ModuleId { get; set; }

        public bool ActionView { get; set; }

        public bool ActionAdd { get; set; }

        public bool ActionEdit { get; set; }

        public bool ActionDelete { get; set; }

        [MaxLength(150)]
        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        [MaxLength(150)]
        public string ModifUser { get; set; }

        public DateTime? ModifDate { get; set; }

        #region Navigation

        public Access Access { get; set; }

        public Profiles Profiles { get; set; }

        public Modules Modules { get; set; }

        #endregion
    }
}
