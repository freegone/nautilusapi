﻿using Microsoft.EntityFrameworkCore;
using Model.DB.Tables;
using Model.DB.Tables.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Model.DB
{
    public class NautilusDB : DbContext
    {
        public NautilusDB(DbContextOptions<NautilusDB> options)
            : base(options)
        {
        }

        public override int SaveChanges()
        {
            CheckTrackInterfaces();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken)
        {
            CheckTrackInterfaces();
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess);
        }

        private void CheckTrackInterfaces()
        {
            var currentDate = DateTime.UtcNow;
            foreach (var entry in ChangeTracker.Entries()
                .Where(e => e.Entity is ICreationDate  && e.State == EntityState.Added))
            {
                var e = entry.Entity as ICreationDate;
                e.CreationDate = currentDate;
            }
        }

        public DbSet<Access> Access { get; set; }

        public DbSet<Agents> Agents { get; set; }

        public DbSet<Companies> Companies { get; set; }

        public DbSet<Concepts> Concepts { get; set; }

        public DbSet<ConceptsDetails> ConceptsDetails { get; set; }

        public DbSet<Costs> Costs { get; set; }

        public DbSet<Customers> Customers { get; set; }

        public DbSet<Departments> Departments { get; set; }

        public DbSet<Modules> Modules { get; set; }

        public DbSet<Offices> Offices { get; set; }

        public DbSet<PaymentMethods> PaymentMethods { get; set; }

        public DbSet<Profiles> Profiles { get; set; }

        public DbSet<ProfilesDetails> ProfilesDetails { get; set; }

        public DbSet<Programs> Programs { get; set; }

        public DbSet<Projects> Projects { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<UsersDetails> UsersDetails { get; set; }

        public DbSet<Vendors> Vendors { get; set; }

        public DbSet<Sales> Sales { get; set; }

        public DbSet<SalesDetails> SalesDetails { get; set; }

        public DbSet<Expenses> Expenses { get; set; }

        public DbSet<ExpensesDetails> ExpensesDetails { get; set; }

        public DbSet<Budgets> Budgets { get; set; }

        public DbSet<BudgetsDetails> BudgetsDetails { get; set; }
    }
}
