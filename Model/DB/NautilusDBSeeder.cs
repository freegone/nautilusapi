﻿using Microsoft.EntityFrameworkCore;

namespace Model.DB
{
    public class NautilusDBSeeder
    {
        private readonly NautilusDB _context;

        public NautilusDBSeeder(NautilusDB context)
        {
            _context = context;
        }

        public void Ensure()
        {
            _context.Database.Migrate();
        }
    }
}
