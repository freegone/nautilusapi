﻿using System;
using System.Collections.Generic;

namespace Global.Responses
{
    public class UserResponse
    {
        public Guid Id { get; set; }

        public Guid ProfileId { get; set; }

        public Guid CompanyId { get; set; }

        public List<Guid> OfficeId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool Status { get; set; }

        public string Picture { get; set; }
    }
}
