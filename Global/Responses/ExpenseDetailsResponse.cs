﻿using System;

namespace Global.Responses
{
    public class ExpenseDetailsResponse
    {
        public DateTime PaidDate { get; set; }

        public int ExpenseTypeId { get; set; }

        public string PaidNumber { get; set; }

        public string PaidImport { get; set; }

        public string Price { get; set; }

        public string TotalPrice { get; set; }

        public string Unit { get; set; }

        public string Counter { get; set; }

        public string Estimated { get; set; }

        public string Retention { get; set; }

        public string Amortization { get; set; }
    }
}
