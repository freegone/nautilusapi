﻿using System;
using System.Collections.Generic;

namespace Global.Responses
{
    public class ProjectResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string CompanyName { get; set; }

        public string OfficeName { get; set; }

        public string Name { get; set; }

        public string Units { get; set; }

        public List<DepartmentResponse> Departments { get; set; }
    }
}
