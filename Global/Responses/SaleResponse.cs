﻿using System;
using System.Collections.Generic;

namespace Global.Responses
{
    public class SaleResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid DepartmentId { get; set; }

        public Guid ProgramId { get; set; }

        public DateTime ContractDate { get; set; }

        public Guid CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string UnitName { get; set; }

        public string Scheme { get; set; }

        public string SalePrice { get; set; }

        public string Paid { get; set; }

        public string Residue { get; set; }

        public string Percentage { get; set; }

        public Guid AgentId { get; set; }

        public string PercentageAgent { get; set; }

        public string ComisionAgent { get; set; }

        public List<SaleDetailsResponse> Sales { get; set; }
    }
}
