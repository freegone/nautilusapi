﻿namespace Global.Responses
{
    public class OverviewResponse
    {
        public string Users { get; set; }

        public string Agents { get; set; }

        public string Companies { get; set; }

        public string Offices { get; set; }
    }
}
