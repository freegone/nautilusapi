﻿using System;
using System.Collections.Generic;

namespace Global.Responses
{
    public class BudgetResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public string CompanyName { get; set; }

        public string OfficeName { get; set; }

        public string ProjectName { get; set; }

        public int TypeId { get; set; }

        public string Projection { get; set; }

        public string Executed { get; set; }

        public string Advance { get; set; }

        public List<BudgetDetailsResponse> Budgets { get; set; }
    }
}
