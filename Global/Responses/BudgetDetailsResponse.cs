﻿using System;

namespace Global.Responses
{
    public class BudgetDetailsResponse
    {
        public int YearId { get; set; }

        public int MonthId { get; set; }

        public Guid ProgramId { get; set; }

        public Guid ConceptId { get; set; }

        public string Total { get; set; }
    }
}
