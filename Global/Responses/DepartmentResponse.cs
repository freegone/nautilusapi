﻿using System;

namespace Global.Responses
{
    public class DepartmentResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public string CompanyName { get; set; }

        public string OfficeName { get; set; }

        public string Unit { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string SalePrice { get; set; }

        public string Characteristics { get; set; }

        public string Status { get; set; }
    }
}
