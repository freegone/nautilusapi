﻿using System;
using System.Collections.Generic;

namespace Global.Responses
{
    public class ExpenseResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public int TypeId { get; set; }

        public string Name { get; set; }

        public DateTime ExpenseDate { get; set; }

        public Guid VendorId { get; set; }

        public Guid ConceptId { get; set; }

        public Guid SubConceptId { get; set; }
        public string VendorName { get; set; }

        public string Contract { get; set; }

        public string Advance { get; set; }

        public string AdvancePercentage { get; set; }

        public string ImssPercentage { get; set; }

        public string Factor { get; set; }

        public string Siroc { get; set; }

        public List<ExpenseDetailsResponse> Expenses { get; set; }
    }
}
