﻿using System;

namespace Global.Responses
{
    public class PaymentMethodResponse
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string CompanyName { get; set; }

        public string OfficeName { get; set; }

        public string Name { get; set; }
    }
}
