﻿using System;

namespace Global.Responses
{
    public class UserDetailsResponse
    {
        public Guid OfficeId { get; set; }
    }
}
