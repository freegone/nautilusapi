﻿using System;

namespace Global.Responses
{
    public class SubConceptResponse
    {
        public Guid Id { get; set; }

        public Guid ConceptId { get; set; }

        public string Name { get; set; }
    }
}
