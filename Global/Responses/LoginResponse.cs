﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Global.Responses
{
    public class LoginResponse
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Picture { get; set; }

        public string Email { get; set; }
    }
}
