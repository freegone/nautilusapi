﻿using System;

namespace Global.Requests
{
    public class VendorRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string Name { get; set; }

        public string RFC { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Notes { get; set; }
    }
}
