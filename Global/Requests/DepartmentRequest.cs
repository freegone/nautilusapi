﻿using System;

namespace Global.Requests
{
    public class DepartmentRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public string Unit { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string SalePrice { get; set; }

        public string Characteristics { get; set; }

        public bool Status { get; set; } = true;
    }
}
