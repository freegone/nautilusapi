﻿using System;

namespace Global.Requests
{
    public class AgentRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string Name { get; set; }

        public string RFC { get; set; }

        public string Phone { get; set; }

        public string Percentaje { get; set; }
    }
}
