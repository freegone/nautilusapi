﻿using System;
using System.Collections.Generic;

namespace Global.Requests
{
    public class ProfileRequest: RequestBase
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool SuperUser { get; set; }

        public Access Access { get; set; }
    }

    public class Access
    {
        public AccessDetails Mobile { get; set; }

        public AccessDetails Web { get; set; }
    }

    public class AccessDetails
    {
        public int AccessId { get; set; }

        public List<Permissions> Permissions { get; set; }
    }

    public class Permissions: RequestBase
    {

        public int ModuleId { get; set; }

        public bool ActionView { get; set; }

        public bool ActionAdd { get; set; }

        public bool ActionEdit { get; set; }

        public bool ActionDelete { get; set; }
    }
}
