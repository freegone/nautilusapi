﻿using System;

namespace Global.Requests
{
    public class OfficeRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Notes { get; set; }

        public string CreatorUser { get; set; }

        public DateTime CreationDate { get; set; }

        public string ModifUser { get; set; }

        public DateTime ModifDate { get; set; }
    }
}
