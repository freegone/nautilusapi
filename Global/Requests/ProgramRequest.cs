﻿using System;

namespace Global.Requests
{
    public class ProgramRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string Name { get; set; }
    }
}
