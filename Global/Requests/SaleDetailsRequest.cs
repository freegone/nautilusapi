﻿using System;

namespace Global.Requests
{
    public class SaleDetailsRequest
    {
        public DateTime PaidDate { get; set; }

        public string PaidImport { get; set; }

        public Guid ConceptId { get; set; }

        public Guid PaymentMethodId { get; set; }

        public int StatusId { get; set; }
    }
}
