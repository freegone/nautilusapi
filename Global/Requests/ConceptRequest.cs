﻿using System;
using System.Collections.Generic;

namespace Global.Requests
{
    public class ConceptRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public int Type { get; set; }

        public string Name { get; set; }

        public List<string> SubConcepts { get; set; }
    }
}
