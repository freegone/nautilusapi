﻿namespace Global.Requests
{
    public class RequestBase
    {
        public RequestBase()
        {
            CreatorUser = "System";
            ModifUser = "System";
        }

        public string CreatorUser { get; set; }

        public string ModifUser { get; set; }
    }
}
