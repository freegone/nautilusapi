﻿using System;
using System.Collections.Generic;

namespace Global.Requests
{
    public class BudgetRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public Guid ProjectId { get; set; }

        public int TypeId { get; set; }

        public string Projection { get; set; }

        public string Executed { get; set; }

        public string Advance { get; set; }

        public List<BudgetDetailsRequest> BudgetDetails { get; set; }
    }
}
