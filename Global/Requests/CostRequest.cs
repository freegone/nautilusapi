﻿using System;

namespace Global.Requests
{
    public class CostRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string level1 { get; set; }

        public string level2 { get; set; }

        public string level3 { get; set; }

        public string level4 { get; set; }
    }
}
