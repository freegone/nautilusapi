﻿using System;

namespace Global.Requests
{
    public class ProjectRequest
    {
        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid OfficeId { get; set; }

        public string Name { get; set; }

        public string Units { get; set; }
    }
}
