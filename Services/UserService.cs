﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public UserService(NautilusDB context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<UserResponse> GetUsers()
        {
            var users = _context.Users.ToList();
            var _users = new List<UserResponse>();

            foreach (var user in users)
            {
                var offices = _context.UsersDetails.Where(x => x.ProfileId == user.Id).Select(x => x.OfficeId).ToList();
                var userResponse = _mapper.Map<UserResponse>(user);
                userResponse.OfficeId = _mapper.Map<List<Guid>>(offices);

                _users.Add(userResponse);
            }

            return _users;
        }

        public UserResponse GetUser(Guid id)
        {
            var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            return _mapper.Map<UserResponse>(user);
        }

        public async Task<UserResponse> SaveUser(UserRequest request)
        {
            var dbRequest = _mapper.Map<Users>(request);
            dbRequest.Password = CreateRandomPassword();
            await _context.AddAsync(dbRequest);


            if (request.OfficeId.Count > 0)
            {
                foreach (var id in request.OfficeId)
                {
                    var usersDetails = new UsersDetails()
                    {
                        ProfileId = dbRequest.Id,
                        OfficeId = id,
                        CreatorUser = request.CreatorUser
                    };

                    await _context.UsersDetails.AddAsync(usersDetails);
                }
            }

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<UserResponse>(dbRequest);
            }

            return null;
        }

        public async Task<UserResponse> UpdateUser(UserRequest request)
        {
            var user = _context.Users.Where(x => x.Id == request.Id).FirstOrDefault();

            if (user == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            user.ProfileId = request.ProfileId;
            user.CompanyId = request.CompanyId;
            user.Name = request.Name;
            user.Email = request.Email;
            user.Phone = request.Phone;
            user.Status = request.Status;
            user.Picture = request.Picture;
            user.ModifUser = request.ModifUser;

            var usersDetails = _context.UsersDetails.Where(x => x.ProfileId == user.Id).ToList();
            _context.UsersDetails.RemoveRange(usersDetails);

            if (request.OfficeId.Count > 0)
            {
                foreach (var id in request.OfficeId)
                {
                    var userDetails = new UsersDetails()
                    {
                        ProfileId = user.Id,
                        OfficeId = id,
                        CreatorUser = request.ModifUser
                    };

                    await _context.UsersDetails.AddAsync(userDetails);
                }
            }

            await _context.SaveChangesAsync();
            return _mapper.Map<UserResponse>(user);
        }

        public async Task<bool> DeleteUser(Guid id)
        {
            var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();
                
            if (user == null)
            {
                return false;
            }

            _context.Users.Remove(user);
            var usersDetails = _context.UsersDetails.Where(x => x.ProfileId == user.Id).ToList();
            _context.UsersDetails.RemoveRange(usersDetails);
            await _context.SaveChangesAsync();
            return true;
        }

        private static string CreateRandomPassword(int length = 15)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }
    }
}
