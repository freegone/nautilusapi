﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class DepartmentsService : IDeparmentsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public DepartmentsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<DepartmentResponse> GetDepartments()
        {
            var departments = _context.Departments.ToList();

            if (departments.Count == 0)
            {
                return null;
            }

            var departmentsResponse = new List<DepartmentResponse>();

            foreach (var department in departments)
            {
                var company = _context.Companies.Where(x => x.Id == department.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == department.OfficeId).FirstOrDefault();
                var departmentResponse = _mapper.Map<DepartmentResponse>(department);
                departmentResponse.CompanyName = company?.Name;
                departmentResponse.OfficeName = office?.Name;
                departmentsResponse.Add(departmentResponse);
            }

            return departmentsResponse;
        }

        public DepartmentResponse GetDepartment(Guid id)
        {
            var departments = _context.Departments.Where(x => x.Id == id).FirstOrDefault();

            if (departments == null)
            {
                return null;
            }

            return _mapper.Map<DepartmentResponse>(departments);
        }

        public async Task<DepartmentResponse> SaveDepartment(DepartmentRequest request)
        {
            var dbRequest = _mapper.Map<Departments>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<DepartmentResponse>(dbRequest);
            }

            return null;
        }

        public async Task<DepartmentResponse> UpdateDepartment(DepartmentRequest request)
        {
            var departments = _context.Departments.Where(x => x.Id == request.Id).FirstOrDefault();

            if (departments == null)
            {
                throw new Exception($"No program was found with id: {request.Id}");
            }

            departments.CompanyId = request.CompanyId;
            departments.OfficeId = request.OfficeId;
            departments.ProjectId = request.ProjectId;
            departments.Unit = request.Unit;
            departments.Type = request.Type;
            departments.Description = request.Description;
            departments.SalePrice = request.SalePrice;
            departments.Characteristics = request.Characteristics;
            departments.Status = request.Status;
            departments.ModifUser = "string";
            departments.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<DepartmentResponse>(departments);
        }

        public async Task<bool?> DeleteDepartment(Guid Id)
        {
            var departments = _context.Departments.Where(x => x.Id == Id).FirstOrDefault();

            if (departments == null)
            {
                return false;
            }

            _context.Departments.Remove(departments);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
