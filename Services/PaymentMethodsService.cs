﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Model.DB.Tables.Interfaces;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class PaymentMethodsService : IPaymentMethodsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public PaymentMethodsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<PaymentMethodResponse> GetPaymentMethods()
        {
            var paymentMethods = _context.PaymentMethods.ToList();

            if (paymentMethods.Count == 0)
            {
                return null;
            }

            var paymentMethodsResponse = new List<PaymentMethodResponse>();

            foreach (var paymentMethod in paymentMethods)
            {
                var company = _context.Companies.Where(x => x.Id == paymentMethod.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == paymentMethod.OfficeId).FirstOrDefault();
                var paymentMethodResponse = _mapper.Map<PaymentMethodResponse>(paymentMethod);
                paymentMethodResponse.CompanyName = company?.Name;
                paymentMethodResponse.OfficeName = office?.Name;
                paymentMethodsResponse.Add(paymentMethodResponse);
            }

            return paymentMethodsResponse;
        }

        public PaymentMethodResponse GetPaymentMethod(Guid id)
        {
            var paymentMethod = _context.PaymentMethods.Where(x => x.Id == id).FirstOrDefault();

            if (paymentMethod == null)
            {
                return null;
            }

            return _mapper.Map<PaymentMethodResponse>(paymentMethod);
        }

        public async Task<PaymentMethodResponse> SavePaymentMethod(PaymentMethodRequest request)
        {
            var dbRequest = _mapper.Map<PaymentMethods>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<PaymentMethodResponse>(dbRequest);
            }

            return null;
        }

        public async Task<PaymentMethodResponse> UpdatePaymentMethod(PaymentMethodRequest request)
        {
            var paymentMethod = _context.PaymentMethods.Where(x => x.Id == request.Id).FirstOrDefault();

            if (paymentMethod == null)
            {
                throw new Exception($"No payment method was found with id: {request.Id}");
            }

            paymentMethod.CompanyId = request.CompanyId;
            paymentMethod.OfficeId = request.OfficeId;
            paymentMethod.Name = request.Name;
            paymentMethod.ModifUser = "string";
            paymentMethod.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<PaymentMethodResponse>(paymentMethod);
        }

        public async Task<bool?> DeletePaymentMethod(Guid Id)
        {
            var paymentMethod = _context.PaymentMethods.Where(x => x.Id == Id).FirstOrDefault();

            if (paymentMethod == null)
            {
                return false;
            }

            _context.PaymentMethods.Remove(paymentMethod);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
