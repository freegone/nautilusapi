﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ProfileService : IProfileService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public ProfileService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<ProfileResponse> GetProfiles()
        {
            var profiles = _context.Profiles.ToList();

            if (profiles.Count == 0)
            {
                return null;
            }

            var profileResponse = _mapper.Map<List<ProfileResponse>>(profiles);

            foreach (var profile in profileResponse)
            {
                var profileDetails = _context.ProfilesDetails.Where(x => x.ProfileId == profile.Id).ToList();

                if (profileDetails != null && profileDetails.Count > 0)
                {
                    profile.Access = new Global.Responses.Access()
                    {
                        Web = new Global.Responses.AccessDetails()
                        {
                            AccessId = 1,
                            Permissions = new List<Global.Responses.Permissions>()
                }
                    };
                    foreach (var profilesDetails in profileDetails)
                    {
                        var permissions = _mapper.Map<Global.Responses.Permissions>(profilesDetails);
                        profile.Access.Web.Permissions.Add(permissions);
                    }
                }
            }

            return profileResponse;
        }

        public ProfileResponse GetProfile(Guid id)
        {
            var company = _context.Companies.Where(x => x.Id == id).FirstOrDefault();

            if (company == null)
            {
                return null;
            }

            return _mapper.Map<ProfileResponse>(company);
        }

        public ProfileResponse GetProfileByUserId(Guid id)
        {
            var userResult = _context.Users.Where(x => x.Id == id).FirstOrDefault();
            var profileResult = _context.Profiles.Where(x => x.Id == userResult.ProfileId).FirstOrDefault();
            var profile = _mapper.Map<ProfileResponse>(profileResult);

            var profileDetails = _context.ProfilesDetails.Where(x => x.ProfileId == profile.Id).ToList();

            if (profileDetails != null && profileDetails.Count > 0)
            {
                profile.Access = new Global.Responses.Access()
                {
                    Web = new Global.Responses.AccessDetails()
                    {
                        AccessId = 1,
                        Permissions = new List<Global.Responses.Permissions>()
                    }
                };
                foreach (var profilesDetails in profileDetails)
                {
                    var permissions = _mapper.Map<Global.Responses.Permissions>(profilesDetails);
                    profile.Access.Web.Permissions.Add(permissions);
                }
            }

            return _mapper.Map<ProfileResponse>(profile);
        }

        public async Task<ProfileResponse> SaveProfile(ProfileRequest request)
        {
            var dbRequest = _mapper.Map<Profiles>(request);
            await _context.AddAsync(dbRequest);

            foreach (var permission in request.Access.Web.Permissions)
            {
                var dbRequestSecond = _mapper.Map<ProfilesDetails>(permission);
                dbRequestSecond.AccessId = request.Access.Web.AccessId;
                dbRequestSecond.ProfileId = dbRequest.Id;
                dbRequestSecond.CreatorUser = request.CreatorUser;
                await _context.AddAsync(dbRequestSecond);
            }
            
            if (await _context.SaveChangesAsync() > 0)
            { 
                return _mapper.Map<ProfileResponse>(dbRequest);
            }

            return null;
        }

        public async Task<ProfileResponse> UpdateProfile(ProfileRequest request)
        {
            var profile = _context.Profiles.Where(x => x.Id == request.Id).FirstOrDefault();

            if (profile == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            profile.Name = request.Name;
            profile.SuperUser = request.SuperUser;
            profile.ModifUser = request.ModifUser;
            profile.ModifDate = DateTime.Now;

            var profileDetailsList = _context.ProfilesDetails.Where(x => x.ProfileId == request.Id).ToList();
            _context.ProfilesDetails.RemoveRange(profileDetailsList);

            foreach (var permission in request.Access.Web.Permissions)
            {
                var profilesDetails = new ProfilesDetails()
                {
                    AccessId = request.Access.Web.AccessId,
                    ProfileId = profile.Id,
                    ModuleId = permission.ModuleId,
                    ActionView = permission.ActionView,
                    ActionAdd = permission.ActionAdd,
                    ActionEdit = permission.ActionEdit,
                    ActionDelete = permission.ActionDelete,
                    CreatorUser = permission.ModifUser
                };

                _context.ProfilesDetails.Add(profilesDetails);
            }

            await _context.SaveChangesAsync();
            return _mapper.Map<ProfileResponse>(profile);
        }

        public async Task<bool?> DeleteProfile(Guid Id)
        {
            var profile = _context.Profiles.Where(x => x.Id == Id).FirstOrDefault();
            var profileDetails = _context.ProfilesDetails.Where(x => x.ProfileId == Id).ToList();

            if (profile == null && profileDetails == null)
            {
                return false;
            }

            var existUsers = _context.Users.Where(x => x.ProfileId == profile.Id).FirstOrDefault();

            if (existUsers != null)
            {
                return null;
            }

            _context.Profiles.Remove(profile);
            _context.ProfilesDetails.RemoveRange(profileDetails);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
