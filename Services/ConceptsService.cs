﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ConceptsService : IConceptsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public ConceptsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<ConceptResponse> GetConcepts()
        {
            var concepts = _context.Concepts.ToList();

            if (concepts.Count == 0)
            {
                return null;
            }

            var conceptsResponse = new List<ConceptResponse>();

            foreach (var concept in concepts)
            {
                var company = _context.Companies.Where(x => x.Id == concept.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == concept.OfficeId).FirstOrDefault();
                var conceptsDetails = _context.ConceptsDetails.Where(x => x.ConceptId == concept.Id).Select(x => new SubConceptResponse() { Id = x.Id, ConceptId = x.ConceptId, Name = x.Name }).OrderBy(x => x.Name).ToList();
                var conceptResponse = _mapper.Map<ConceptResponse>(concept);
                conceptResponse.CompanyName = company?.Name;
                conceptResponse.OfficeName = office?.Name;
                conceptResponse.SubConcepts = conceptsDetails;
                conceptsResponse.Add(conceptResponse);
            }

            return conceptsResponse;
        }

        public ConceptResponse GetConcept(Guid id)
        {
            var concept = _context.Concepts.Where(x => x.Id == id).FirstOrDefault();

            if (concept == null)
            {
                return null;
            }

            var conceptResponse = _mapper.Map<ConceptResponse>(concept);
            var company = _context.Companies.Where(x => x.Id == concept.CompanyId).FirstOrDefault();
            var office = _context.Offices.Where(x => x.Id == concept.OfficeId).FirstOrDefault();
            var conceptsDetails = _context.ConceptsDetails.Where(x => x.ConceptId == concept.Id).Select(x => new SubConceptResponse() { Id = x.Id, ConceptId = x.ConceptId, Name = x.Name }).OrderBy(x => x).ToList();
            conceptResponse.CompanyName = company?.Name;
            conceptResponse.OfficeName = office?.Name;
            conceptResponse.SubConcepts = conceptsDetails;

            return _mapper.Map<ConceptResponse>(concept);
        }

        public async Task<ConceptResponse> SaveConcept(ConceptRequest request)
        {
            var dbRequest = _mapper.Map<Concepts>(request); 
            await _context.AddAsync(dbRequest);

            if (request.SubConcepts.Count > 0)
            {
                foreach (var subConcept in request.SubConcepts)
                {
                    var conceptDetail = new ConceptsDetails()
                    {
                        ConceptId = dbRequest.Id,
                        Name = subConcept
                    };

                    _context.ConceptsDetails.Add(conceptDetail);
                }
            }

            if (await _context.SaveChangesAsync() > 0)
            {
                var conceptResponse = _mapper.Map<ConceptResponse>(dbRequest);
                var conceptsDetails = _context.ConceptsDetails.Where(x => x.ConceptId == dbRequest.Id).Select(x => new SubConceptResponse() { Id = x.Id, ConceptId = x.ConceptId, Name = x.Name }).OrderBy(x => x.Name).ToList();

                conceptResponse.SubConcepts = conceptsDetails;

                return conceptResponse;
            }

            return null;
        }

        public async Task<ConceptResponse> UpdateConcept(ConceptRequest request)
        {
            var concept = _context.Concepts.Where(x => x.Id == request.Id).FirstOrDefault();

            if (concept == null)
            {
                throw new Exception($"No concept was found with id: {request.Id}");
            }

            concept.CompanyId = request.CompanyId;
            concept.OfficeId = request.OfficeId;
            concept.Name = request.Name;
            concept.Type = request.Type;
            concept.ModifUser = "string";
            concept.ModifDate = DateTime.Now;

            var subConcepts = _context.ConceptsDetails.Where(x => x.ConceptId == concept.Id).ToList();
            _context.ConceptsDetails.RemoveRange(subConcepts);
            
            if (request.SubConcepts.Count > 0)
            {
                foreach (var subConcept in request.SubConcepts)
                {
                    var conceptDetail = new ConceptsDetails()
                    {
                        ConceptId = concept.Id,
                        Name = subConcept
                    };

                    _context.ConceptsDetails.Add(conceptDetail);
                }
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<ConceptResponse>(concept);
        }

        public async Task<bool?> DeleteConcept(Guid Id)
        {
            var concept = _context.Concepts.Where(x => x.Id == Id).FirstOrDefault();

            if (concept == null)
            {
                return false;
            }

            _context.Concepts.Remove(concept);
            var subConcepts = _context.ConceptsDetails.Where(x => x.ConceptId == concept.Id).ToList();
            _context.ConceptsDetails.RemoveRange(subConcepts);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
