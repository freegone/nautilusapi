﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class BudgetsService : IBudgetsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public BudgetsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<BudgetResponse> GetBudgets()
        {
            var budgets = _context.Budgets.ToList();

            if (budgets.Count == 0)
            {
                return null;
            }

            var budgetResponses = new List<BudgetResponse>();

            foreach (var budget in budgets)
            {
                var company = _context.Companies.Where(x => x.Id == budget.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == budget.OfficeId).FirstOrDefault();
                var project = _context.Projects.Where(x => x.Id == budget.ProjectId).FirstOrDefault();
                var budgetDetails = _context.BudgetsDetails.Where(x => x.BudgetId == budget.Id).Select(x => _mapper.Map<BudgetDetailsResponse>(x)).ToList();
                var budgetResponse = _mapper.Map<BudgetResponse>(budget);
                budgetResponse.CompanyName = company.Name;
                budgetResponse.OfficeName = office.Name;
                budgetResponse.ProjectName = project.Name;
                budgetResponse.Budgets = budgetDetails;

                budgetResponses.Add(budgetResponse);
            }

            return budgetResponses;
        }

        public BudgetResponse GetBudget(Guid id)
        {
            var budget = _context.Budgets.Where(x => x.Id == id).FirstOrDefault();

            if (budget == null)
            {
                return null;
            }

            return _mapper.Map<BudgetResponse>(budget);
        }

        public async Task<BudgetResponse> SaveBudget(BudgetRequest request)
        {
            var dbRequest = _mapper.Map<Budgets>(request);
            await _context.AddAsync(dbRequest);

            if (request.BudgetDetails.Count > 0)
            {
                foreach (var budgetDetail in request.BudgetDetails)
                {
                    var budgetsDetails = new BudgetsDetails()
                    {
                        BudgetId = dbRequest.Id,
                        YearId = budgetDetail.YearId,
                        MonthId = budgetDetail.MonthId,
                        ProgramId = budgetDetail.ProgramId,
                        ConceptId = budgetDetail.ConceptId,
                        Total = budgetDetail.Total,
                        CreatorUser = "roberto_gb23@hotmail.com"
                    };

                    await _context.BudgetsDetails.AddAsync(budgetsDetails);
                }
            }

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<BudgetResponse>(dbRequest);
            }

            return null;
        }

        public async Task<BudgetResponse> UpdateBudget(BudgetRequest request)
        {
            var budget = _context.Budgets.Where(x => x.Id == request.Id).FirstOrDefault();

            if (budget == null)
            {
                throw new Exception($"No budget was found with id: {request.Id}");
            }

            budget.CompanyId = request.CompanyId;
            budget.OfficeId = request.OfficeId;
            budget.ProjectId = request.ProjectId;
            budget.TypeId = request.TypeId;
            budget.Projection = request.Projection;
            budget.Executed = request.Executed;
            budget.Advance = request.Advance;
            budget.ModifUser = "string";
            budget.ModifDate = DateTime.Now;

            var budgetsDetailsList = _context.BudgetsDetails.Where(x => x.BudgetId == budget.Id).ToList();
            _context.BudgetsDetails.RemoveRange(budgetsDetailsList);

            if (request.BudgetDetails.Count > 0)
            {
                foreach (var budgetDetail in request.BudgetDetails)
                {
                    var budgetsDetails = new BudgetsDetails()
                    {
                        BudgetId = budget.Id,
                        YearId = budgetDetail.YearId,
                        MonthId = budgetDetail.MonthId,
                        ProgramId = budgetDetail.ProgramId,
                        ConceptId = budgetDetail.ConceptId,
                        Total = budgetDetail.Total,
                        ModifUser = "roberto_gb23@hotmail.com",
                        ModifDate = DateTime.Now
                    };

                    _context.BudgetsDetails.Add(budgetsDetails);
                }
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<BudgetResponse>(budget);
        }

        public async Task<bool?> DeleteBudget(Guid Id)
        {
            var budget = _context.Budgets.Where(x => x.Id == Id).FirstOrDefault();

            if (budget == null)
            {
                return false;
            }

            _context.Budgets.Remove(budget);
            var budgetsDetails = _context.BudgetsDetails.Where(x => x.BudgetId == Id).ToList();
            _context.BudgetsDetails.RemoveRange(budgetsDetails);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
