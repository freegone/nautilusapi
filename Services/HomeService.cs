﻿using Global.Responses;
using Model.DB;
using Services.Interfaces;
using System.Linq;

namespace Services
{
    public class HomeService : IHomeService
    {
        private readonly NautilusDB _context;

        public HomeService(NautilusDB context)
        {
            _context = context;
        }

        public OverviewResponse GetOverview()
        {
            return new OverviewResponse()
            {
                Users = _context.Users.ToList().Count().ToString(),
                Agents = _context.Agents.ToList().Count().ToString(),
                Companies = _context.Companies.ToList().Count().ToString(),
                Offices = _context.Offices.ToList().Count().ToString()
            };
        }
    }
}
