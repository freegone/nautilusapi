﻿using AutoMapper;
using Global.Requests;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class CustomersService : ICustomersService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public CustomersService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<CustomerResponse> GetCustomers()
        {
            var customers = _context.Customers.ToList();

            if (customers.Count == 0)
            {
                return null;
            }

            var customersResponse = new List<CustomerResponse>();

            foreach (var customer in customers)
            {
                var company = _context.Companies.Where(x => x.Id == customer.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == customer.OfficeId).FirstOrDefault();
                var customerResponse = _mapper.Map<CustomerResponse>(customer);
                customerResponse.CompanyName = company?.Name;
                customerResponse.OfficeName = office?.Name;
                customersResponse.Add(customerResponse);
            }

            return customersResponse;
        }

        public CustomerResponse GetCustomer(Guid id)
        {
            var customer = _context.Customers.Where(x => x.Id == id).FirstOrDefault();

            if (customer == null)
            {
                return null;
            }

            return _mapper.Map<CustomerResponse>(customer);
        }

        public async Task<CustomerResponse> SaveCustomer(CustomerRequest request)
        {
            var dbRequest = _mapper.Map<Customers>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CustomerResponse>(dbRequest);
            }

            return null;
        }

        public async Task<CustomerResponse> UpdateCustomer(CustomerRequest request)
        {
            var customer = _context.Customers.Where(x => x.Id == request.Id).FirstOrDefault();

            if (customer == null)
            {
                throw new Exception($"No customer was found with id: {request.Id}");
            }

            customer.CompanyId = request.CompanyId;
            customer.OfficeId = request.OfficeId;
            customer.Name = request.Name;
            customer.RFC = request.RFC;
            customer.Email = request.Email;
            customer.Phone = request.Phone;
            customer.Address = request.Address;
            customer.Notes = request.Notes;
            customer.CreatorUser = "example@hotmail.com";
            customer.CreationDate = DateTime.Now;
            customer.ModifUser = null;
            customer.ModifDate = null;

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CustomerResponse>(customer);
            }

            return null;
        }

        public async Task<bool?> DeleteCustomer(Guid Id)
        {
            var customer = _context.Customers.Where(x => x.Id == Id).FirstOrDefault();

            if (customer == null)
            {
                return false;
            }

            var existUsers = _context.Offices.Where(x => x.CompanyId == customer.Id).FirstOrDefault();

            if (existUsers != null)
            {
                return null;
            }

            _context.Customers.Remove(customer);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
