﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class CostsService : ICostsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public CostsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<CostResponse> GetCosts()
        {
            var costs = _context.Costs.ToList();

            if (costs.Count == 0)
            {
                return null;
            }

            var costsResponse = new List<CostResponse>();

            foreach (var cost in costs)
            {
                var company = _context.Companies.Where(x => x.Id == cost.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == cost.OfficeId).FirstOrDefault();
                var costResponse = _mapper.Map<CostResponse>(cost);
                costResponse.CompanyName = company?.Name;
                costResponse.OfficeName = office?.Name;
                costsResponse.Add(costResponse);
            }

            return costsResponse;
        }

        public CostResponse GetCost(Guid id)
        {
            var costs = _context.Costs.Where(x => x.Id == id).FirstOrDefault();

            if (costs == null)
            {
                return null;
            }

            return _mapper.Map<CostResponse>(costs);
        }

        public async Task<CostResponse> SaveCost(CostRequest request)
        {
            var dbRequest = _mapper.Map<Costs>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CostResponse>(dbRequest);
            }

            return null;
        }

        public async Task<CostResponse> UpdateCost(CostRequest request)
        {
            var costs = _context.Costs.Where(x => x.Id == request.Id).FirstOrDefault();

            if (costs == null)
            {
                throw new Exception($"No program was found with id: {request.Id}");
            }

            costs.CompanyId = request.CompanyId;
            costs.OfficeId = request.OfficeId;
            costs.Level1 = request.level1;
            costs.Level2 = request.level2;
            costs.Level3 = request.level3;
            costs.Level4 = request.level4;
            costs.ModifUser = "string";
            costs.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<CostResponse>(costs);
        }

        public async Task<bool?> DeleteCost(Guid Id)
        {
            var cost = _context.Costs.Where(x => x.Id == Id).FirstOrDefault();

            if (cost == null)
            {
                return false;
            }

            _context.Costs.Remove(cost);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
