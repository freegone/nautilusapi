﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class OfficesService : IOfficesServices
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public OfficesService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<OfficeResponse> GetOffices()
        {
            var offices = _context.Offices.ToList();

            if (offices.Count == 0)
            {
                return null;
            }

            var officesResponse = new List<OfficeResponse>();

            foreach (var office in offices)
            {
                var company = _context.Companies.Where(x => x.Id == office.CompanyId).FirstOrDefault();
                var officeResponse = _mapper.Map<OfficeResponse>(office);
                officeResponse.CompanyName = company?.Name;
                officesResponse.Add(officeResponse);
            }

            return officesResponse;
        }

        public OfficeResponse GetOffice(Guid id)
        {
            var office = _context.Offices.Where(x => x.Id == id).FirstOrDefault();

            if (office == null)
            {
                return null;
            }

            return _mapper.Map<OfficeResponse>(office);
        }

        public async Task<OfficeResponse> SaveOffice(OfficeRequest request)
        {
            var dbRequest = _mapper.Map<Offices>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<OfficeResponse>(dbRequest);
            }

            return null;
        }

        public async Task<OfficeResponse> UpdateOffice(OfficeRequest request)
        {
            var office = _context.Offices.Where(x => x.Id == request.Id).FirstOrDefault();

            if (office == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            var existUsers = _context.Users.Where(x => x.CompanyId == office.CompanyId).FirstOrDefault();

            if (office.CompanyId != request.CompanyId && existUsers != null)
            {
                return null;
            }

            office.Name = request.Name;
            office.Address = request.Address;
            office.Notes = request.Notes;
            office.CompanyId = request.CompanyId;
            office.CreatorUser = request.CreatorUser;
            office.CreationDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<OfficeResponse>(office);
        }

        public async Task<bool?> DeleteOffice(Guid Id)
        {
            var office = _context.Offices.Where(x => x.Id == Id).FirstOrDefault();

            if (office == null)
            {
                return false;
            }

            var existUsers = _context.UsersDetails.Where(x => x.OfficeId == office.Id).FirstOrDefault();

            if (existUsers != null)
            {
                return null;
            }

            _context.Offices.Remove(office);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
