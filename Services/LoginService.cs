﻿using AutoMapper;
using Global;
using Global.Responses;
using Model.DB;
using Services.Interfaces;
using System.Linq;

namespace Services
{
    public class LoginService : ILoginService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public LoginService(NautilusDB context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public LoginResponse DoLogin(LoginRequest request)
        {
            var user = _context.Users.Where(u => u.Email == request.Email && u.Password == request.Password).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            return _mapper.Map<LoginResponse>(user);
        }
    }
}
