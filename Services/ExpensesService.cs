﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ExpensesService : IExpensesService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public ExpensesService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<ExpenseResponse> GetExpenses()
        {
            var expenses = _context.Expenses.ToList();

            if (expenses.Count == 0)
            {
                return null;
            }

            var expenseResponses = new List<ExpenseResponse>();

            foreach (var expense in expenses)
            {
                var company = _context.Companies.Where(x => x.Id == expense.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == expense.OfficeId).FirstOrDefault();
                var vendor = _context.Vendors.Where(x => x.Id == expense.VendorId).FirstOrDefault();
                var expenseDetails = _context.ExpensesDetails.Where(x => x.ExpenseId == expense.Id).OrderBy(x => x.PaidDate).Select(x => _mapper.Map<ExpenseDetailsResponse>(x)).ToList();
                var expenseResponse = _mapper.Map<ExpenseResponse>(expense);
                expenseResponse.VendorName = vendor.Name;
                expenseResponse.Expenses = expenseDetails;
                expenseResponse.Name = expense.TypeId == 1 ? "Egreso" : "Contrato";

                expenseResponses.Add(expenseResponse);
            }

            return expenseResponses;
        }

        public ExpenseResponse GetExpense(Guid id)
        {
            var expense = _context.Expenses.Where(x => x.Id == id).FirstOrDefault();

            if (expense == null)
            {
                return null;
            }

            return _mapper.Map<ExpenseResponse>(expense);
        }

        public async Task<ExpenseResponse> SaveExpense(ExpenseRequest request)
        {
            var dbRequest = _mapper.Map<Expenses>(request);
            await _context.AddAsync(dbRequest);

            if (request.ExpenseDetails.Count > 0)
            {
                foreach (var expenseDetail in request.ExpenseDetails)
                {
                    var expensesDetails = new ExpensesDetails()
                    {
                        ExpenseId = dbRequest.Id,
                        PaidDate = expenseDetail.PaidDate,
                        ExpenseTypeId = expenseDetail.ExpenseTypeId,
                        PaidNumber = expenseDetail.PaidNumber,
                        PaidImport = expenseDetail.PaidImport,
                        Price = expenseDetail.Price,
                        TotalPrice = expenseDetail.TotalPrice,
                        Unit = expenseDetail.Unit,
                        Counter = expenseDetail.Counter,
                        Estimated = expenseDetail.Estimated,
                        Retention = expenseDetail.Retention,
                        Amortization = expenseDetail.Amortization,
                        CreatorUser = "roberto_gb23@hotmail.com"
                    };

                    await _context.ExpensesDetails.AddAsync(expensesDetails);
                }
            }

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<ExpenseResponse>(dbRequest);
            }

            return null;
        }

        public async Task<ExpenseResponse> UpdateExpense(ExpenseRequest request)
        {
            var expense = _context.Expenses.Where(x => x.Id == request.Id).FirstOrDefault();

            if (expense == null)
            {
                throw new Exception($"No expense was found with id: {request.Id}");
            }

            expense.CompanyId = request.CompanyId;
            expense.OfficeId = request.OfficeId;
            expense.ProjectId = request.ProjectId;
            expense.TypeId = request.TypeId;
            expense.ExpenseDate = request.ExpenseDate;
            expense.VendorId = request.VendorId;
            expense.ConceptId = request.ConceptId;
            expense.SubConceptId = request.SubConceptId;
            expense.Contract = request.Contract;
            expense.Advance = request.Advance;
            expense.AdvancePercentage = request.AdvancePercentage;
            expense.IMSSPercentage = request.ImssPercentage;
            expense.Factor = request.Factor;
            expense.Siroc = request.Siroc;
            expense.ModifUser = "string";
            expense.ModifDate = DateTime.Now;

            var expensesDetailsList = _context.ExpensesDetails.Where(x => x.ExpenseId == expense.Id).ToList();
            _context.ExpensesDetails.RemoveRange(expensesDetailsList);

            if (request.ExpenseDetails.Count > 0)
            {
                foreach (var expenseDetail in request.ExpenseDetails)
                {
                    var expenseDetails = new ExpensesDetails()
                    {
                        ExpenseId = expense.Id,
                        PaidDate = expenseDetail.PaidDate,
                        ExpenseTypeId = expenseDetail.ExpenseTypeId,
                        PaidNumber = expenseDetail.PaidNumber,
                        PaidImport = expenseDetail.PaidImport,
                        Price = expenseDetail.Price,
                        TotalPrice = expenseDetail.TotalPrice,
                        Unit = expenseDetail.Unit,
                        Counter = expenseDetail.Counter,
                        Estimated = expenseDetail.Estimated,
                        Retention = expenseDetail.Retention,
                        Amortization = expenseDetail.Amortization,
                        ModifUser = "roberto_gb23@hotmail.com",
                        ModifDate = DateTime.Now
                };

                    _context.ExpensesDetails.Add(expenseDetails);
                }
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<ExpenseResponse>(expense);
        }

        public async Task<bool?> DeleteExpense(Guid Id)
        {
            var expense = _context.Expenses.Where(x => x.Id == Id).FirstOrDefault();

            if (expense == null)
            {
                return false;
            }

            _context.Expenses.Remove(expense);
            var expensesDetails = _context.ExpensesDetails.Where(x => x.ExpenseId == Id).ToList();
            _context.ExpensesDetails.RemoveRange(expensesDetails);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
