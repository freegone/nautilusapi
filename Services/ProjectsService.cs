﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public ProjectsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<ProjectResponse> GetProjects(bool allDepartments)
        {
            var projects = _context.Projects.ToList();

            if (projects.Count == 0)
            {
                return null;
            }

            var projectsResponse = new List<ProjectResponse>();

            foreach (var project in projects)
            {
                var company = _context.Companies.Where(x => x.Id == project.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == project.OfficeId).FirstOrDefault();
                var departments = new List<Departments>();
                if (allDepartments)
                {
                    departments = _context.Departments.Where(x => x.ProjectId == project.Id).ToList();
                }
                else
                {
                    departments = _context.Departments.Where(x => x.ProjectId == project.Id && !_context.Sales.Any(y => y.DepartmentId == x.Id)).ToList();
                }

                var projectResponse = _mapper.Map<ProjectResponse>(project);
                projectResponse.CompanyName = company?.Name;
                projectResponse.OfficeName = office?.Name;
                projectResponse.Departments = _mapper.Map<List<DepartmentResponse>>(departments);
                projectsResponse.Add(projectResponse);
            }

            return projectsResponse;
        }

        public ProjectResponse GetProject(Guid id)
        {
            var project = _context.Projects.Where(x => x.Id == id).FirstOrDefault();

            if (project == null)
            {
                return null;
            }

            return _mapper.Map<ProjectResponse>(project);
        }

        public async Task<ProjectResponse> SaveProject(ProjectRequest request)
        {
            var dbRequest = _mapper.Map<Projects>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<ProjectResponse>(dbRequest);
            }

            return null;
        }

        public async Task<ProjectResponse> UpdateProject(ProjectRequest request)
        {
            var project = _context.Projects.Where(x => x.Id == request.Id).FirstOrDefault();

            if (project == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            project.CompanyId = request.CompanyId;
            project.OfficeId = request.OfficeId;
            project.Name = request.Name;
            project.Units = request.Units;
            project.ModifUser = "string";
            project.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<ProjectResponse>(project);
        }

        public async Task<bool?> DeleteProject(Guid Id)
        {
            var project = _context.Projects.Where(x => x.Id == Id).FirstOrDefault();

            if (project == null)
            {
                return false;
            }

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
