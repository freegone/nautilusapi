﻿using AutoMapper;
using Global.Requests;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class CompaniesService : ICompaniesService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public CompaniesService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<CompanyResponse> GetCompanies()
        {
            var companies = _context.Companies.ToList();
            
            if (companies.Count == 0)
            {
                return null;
            }

            return _mapper.Map<List<CompanyResponse>>(companies);
        }

        public CompanyResponse GetCompany(Guid id)
        {
            var company = _context.Companies.Where(x => x.Id == id).FirstOrDefault();

            if (company == null)
            {
                return null;
            }

            return _mapper.Map<CompanyResponse>(company);
        }

        public async Task<CompanyResponse> SaveCompany(CompanyRequest request)
        {
            var dbRequest = _mapper.Map<Companies>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CompanyResponse>(dbRequest);
            }

            return null;
        }

        public async Task<CompanyResponse> UpdateCompany(CompanyRequest request)
        {
            var company = _context.Companies.Where(x => x.Id == request.Id).FirstOrDefault();

            if (company == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            company.Name = request.Name;
            company.RFC = request.RFC;
            company.Email = request.Email;
            company.Phone = request.Phone;
            company.Address = request.Address;
            company.Notes = request.Notes;
            company.CreatorUser = "example@hotmail.com";
            company.CreationDate = DateTime.Now;
            company.ModifUser = null;
            company.ModifDate = null;

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CompanyResponse>(company);
            }

            return null;
        }

        public async Task<bool?> DeleteCompany(Guid Id)
        {
            var company = _context.Companies.Where(x => x.Id == Id).FirstOrDefault();

            if (company == null)
            {
                return false;
            }

            var existUsers = _context.Offices.Where(x => x.CompanyId == company.Id).FirstOrDefault();

            if (existUsers != null)
            {
                return null;
            }

            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
