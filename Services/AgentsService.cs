﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class AgentsService : IAgentsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public AgentsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<AgentResponse> GetAgents()
        {
            var agents = _context.Agents.ToList();

            if (agents.Count == 0)
            {
                return null;
            }

            var agentsResponse = new List<AgentResponse>();

            foreach (var agent in agents)
            {
                var company = _context.Companies.Where(x => x.Id == agent.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == agent.OfficeId).FirstOrDefault();
                var agentResponse = _mapper.Map<AgentResponse>(agent);
                agentResponse.CompanyName = company?.Name;
                agentResponse.OfficeName = office?.Name;
                agentsResponse.Add(agentResponse);
            }

            return agentsResponse;
        }

        public AgentResponse GetAgent(Guid id)
        {
            var agent = _context.Agents.Where(x => x.Id == id).FirstOrDefault();

            if (agent == null)
            {
                return null;
            }

            return _mapper.Map<AgentResponse>(agent);
        }

        public async Task<AgentResponse> SaveAgent(AgentRequest request)
        {
            var dbRequest = _mapper.Map<Agents>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<AgentResponse>(dbRequest);
            }

            return null;
        }

        public async Task<AgentResponse> UpdateAgent(AgentRequest request)
        {
            var agent = _context.Agents.Where(x => x.Id == request.Id).FirstOrDefault();

            if (agent == null)
            {
                throw new Exception($"No agent was found with id: {request.Id}");
            }

            agent.CompanyId = request.CompanyId;
            agent.OfficeId = request.OfficeId;
            agent.Name = request.Name;
            agent.Phone = request.Phone;
            agent.RFC = request.RFC;
            agent.Percentaje = request.Percentaje;
            agent.ModifUser = "string";
            agent.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<AgentResponse>(agent);
        }

        public async Task<bool?> DeleteAgent(Guid Id)
        {
            var agent = _context.Agents.Where(x => x.Id == Id).FirstOrDefault();

            if (agent == null)
            {
                return false;
            }

            _context.Agents.Remove(agent);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
