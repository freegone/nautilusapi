﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IConceptsService
    {
        List<ConceptResponse> GetConcepts();

        ConceptResponse GetConcept(Guid id);

        Task<ConceptResponse> SaveConcept(ConceptRequest request);

        Task<ConceptResponse> UpdateConcept(ConceptRequest request);

        Task<bool?> DeleteConcept(Guid id);
    }
}
