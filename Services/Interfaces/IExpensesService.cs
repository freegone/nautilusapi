﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ISalesService
    {
        List<SaleResponse> GetSales();

        SaleResponse GetSale(Guid id);

        Task<SaleResponse> SaveSale(SaleRequest request);

        Task<SaleResponse> UpdateSale(SaleRequest request);

        Task<bool?> DeleteSale(Guid id);
    }
}
