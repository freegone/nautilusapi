﻿using Global.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IVendorsService
    {
        List<VendorResponse> GetVendors();

        VendorResponse GetVendor(Guid id);

        Task<VendorResponse> SaveVendor(VendorRequest request);

        Task<VendorResponse> UpdateVendor(VendorRequest request);

        Task<bool?> DeleteVendor(Guid id);
    }
}
