﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IBudgetsService
    {
        List<BudgetResponse> GetBudgets();

        BudgetResponse GetBudget(Guid id);

        Task<BudgetResponse> SaveBudget(BudgetRequest request);

        Task<BudgetResponse> UpdateBudget(BudgetRequest request);

        Task<bool?> DeleteBudget(Guid id);
    }
}
