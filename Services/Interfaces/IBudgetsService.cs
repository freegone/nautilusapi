﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IExpensesService
    {
        List<ExpenseResponse> GetExpenses();

        ExpenseResponse GetExpense(Guid id);

        Task<ExpenseResponse> SaveExpense(ExpenseRequest request);

        Task<ExpenseResponse> UpdateExpense(ExpenseRequest request);

        Task<bool?> DeleteExpense(Guid id);
    }
}
