﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IUserService
    {
        List<UserResponse> GetUsers();

        UserResponse GetUser(Guid id);

        Task<UserResponse> SaveUser(UserRequest request);

        Task<UserResponse> UpdateUser(UserRequest request);

        Task<bool> DeleteUser(Guid id);
    }
}
