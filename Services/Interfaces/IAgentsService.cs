﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IAgentsService
    {
        List<AgentResponse> GetAgents();

        AgentResponse GetAgent(Guid id);

        Task<AgentResponse> SaveAgent(AgentRequest request);

        Task<AgentResponse> UpdateAgent(AgentRequest request);

        Task<bool?> DeleteAgent(Guid id);
    }
}
