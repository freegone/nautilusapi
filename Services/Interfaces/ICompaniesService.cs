﻿using Global.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICompaniesService
    {
        List<CompanyResponse> GetCompanies();

        CompanyResponse GetCompany(Guid id);

        Task<CompanyResponse> SaveCompany(CompanyRequest request);

        Task<CompanyResponse> UpdateCompany(CompanyRequest request);

        Task<bool?> DeleteCompany(Guid id);
    }
}
