﻿using Global.Responses;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IHomeService
    {
        OverviewResponse GetOverview();
    }
}
