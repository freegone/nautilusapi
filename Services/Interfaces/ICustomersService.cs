﻿using Global.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICustomersService
    {
        List<CustomerResponse> GetCustomers();

        CustomerResponse GetCustomer(Guid id);

        Task<CustomerResponse> SaveCustomer(CustomerRequest request);

        Task<CustomerResponse> UpdateCustomer(CustomerRequest request);

        Task<bool?> DeleteCustomer(Guid id);
    }
}
