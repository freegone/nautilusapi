﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IPaymentMethodsService
    {
        List<PaymentMethodResponse> GetPaymentMethods();

        PaymentMethodResponse GetPaymentMethod(Guid id);

        Task<PaymentMethodResponse> SavePaymentMethod(PaymentMethodRequest request);

        Task<PaymentMethodResponse> UpdatePaymentMethod(PaymentMethodRequest request);

        Task<bool?> DeletePaymentMethod(Guid id);
    }
}
