﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IOfficesServices
    {
        List<OfficeResponse> GetOffices();

        OfficeResponse GetOffice(Guid id);

        Task<OfficeResponse> SaveOffice(OfficeRequest request);

        Task<OfficeResponse> UpdateOffice(OfficeRequest request);

        Task<bool?> DeleteOffice(Guid id);
    }
}
