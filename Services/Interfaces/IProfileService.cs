﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IProfileService
    {
        List<ProfileResponse> GetProfiles();

        ProfileResponse GetProfile(Guid id);

        ProfileResponse GetProfileByUserId(Guid id);

        Task<ProfileResponse> SaveProfile(ProfileRequest request);

        Task<ProfileResponse> UpdateProfile(ProfileRequest request);

        Task<bool?> DeleteProfile(Guid id);
    }
}
