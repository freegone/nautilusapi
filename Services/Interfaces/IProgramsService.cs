﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IProgramsService
    {
        List<ProgramResponse> GetPrograms();

        ProgramResponse GetProgram(Guid id);

        Task<ProgramResponse> SaveProgram(ProgramRequest request);

        Task<ProgramResponse> UpdateProgram(ProgramRequest request);

        Task<bool?> DeleteProgram(Guid id);
    }
}
