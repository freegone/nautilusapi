﻿using Global;
using Global.Responses;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ILoginService
    {
        LoginResponse DoLogin(LoginRequest loginRequest);
    }
}
