﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IProjectsService
    {
        List<ProjectResponse> GetProjects(bool allDepartments);

        ProjectResponse GetProject(Guid id);

        Task<ProjectResponse> SaveProject(ProjectRequest request);

        Task<ProjectResponse> UpdateProject(ProjectRequest request);

        Task<bool?> DeleteProject(Guid id);
    }
}
