﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICostsService
    {
        List<CostResponse> GetCosts();

        CostResponse GetCost(Guid id);

        Task<CostResponse> SaveCost(CostRequest request);

        Task<CostResponse> UpdateCost(CostRequest request);

        Task<bool?> DeleteCost(Guid id);
    }
}
