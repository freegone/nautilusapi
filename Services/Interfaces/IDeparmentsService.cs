﻿using Global.Requests;
using Global.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDeparmentsService
    {
        List<DepartmentResponse> GetDepartments();

        DepartmentResponse GetDepartment(Guid id);

        Task<DepartmentResponse> SaveDepartment(DepartmentRequest request);

        Task<DepartmentResponse> UpdateDepartment(DepartmentRequest request);

        Task<bool?> DeleteDepartment(Guid id);
    }
}
