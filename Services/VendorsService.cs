﻿using AutoMapper;
using Global.Requests;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class VendorsService : IVendorsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public VendorsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<VendorResponse> GetVendors()
        {
            var vendors = _context.Vendors.ToList();

            if (vendors.Count == 0)
            {
                return null;
            }

            var vendorsResponse = new List<VendorResponse>();

            foreach (var program in vendors)
            {
                var company = _context.Companies.Where(x => x.Id == program.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == program.OfficeId).FirstOrDefault();
                var vendorResponse = _mapper.Map<VendorResponse>(program);
                vendorResponse.CompanyName = company?.Name;
                vendorResponse.OfficeName = office?.Name;
                vendorsResponse.Add(vendorResponse);
            }

            return vendorsResponse;
        }

        public VendorResponse GetVendor(Guid id)
        {
            var vendor = _context.Vendors.Where(x => x.Id == id).FirstOrDefault();

            if (vendor == null)
            {
                return null;
            }

            return _mapper.Map<VendorResponse>(vendor);
        }

        public async Task<VendorResponse> SaveVendor(VendorRequest request)
        {
            var dbRequest = _mapper.Map<Vendors>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<VendorResponse>(dbRequest);
            }

            return null;
        }

        public async Task<VendorResponse> UpdateVendor(VendorRequest request)
        {
            var vendor = _context.Vendors.Where(x => x.Id == request.Id).FirstOrDefault();

            if (vendor == null)
            {
                throw new Exception($"No user was found with id: {request.Id}");
            }

            vendor.CompanyId = request.CompanyId;
            vendor.OfficeId = request.OfficeId;
            vendor.Name = request.Name;
            vendor.RFC = request.RFC;
            vendor.Email = request.Email;
            vendor.Phone = request.Phone;
            vendor.Address = request.Address;
            vendor.Notes = request.Notes;
            vendor.CreatorUser = "example@hotmail.com";
            vendor.CreationDate = DateTime.Now;
            vendor.ModifUser = null;
            vendor.ModifDate = null;

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<VendorResponse>(vendor);
            }

            return null;
        }

        public async Task<bool?> DeleteVendor(Guid Id)
        {
            var vendor = _context.Vendors.Where(x => x.Id == Id).FirstOrDefault();

            if (vendor == null)
            {
                return false;
            }

            var existUsers = _context.Offices.Where(x => x.CompanyId == vendor.Id).FirstOrDefault();

            if (existUsers != null)
            {
                return null;
            }

            _context.Vendors.Remove(vendor);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
