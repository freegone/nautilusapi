﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model;
using Model.DB.Tables;
using Model.DB.Tables.Interfaces;

namespace Services.AutoMapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMaps();
        }

        public void CreateMaps()
        {
            CreateMap<Users, LoginResponse>();

            CreateMap<Users, UserResponse>();
            CreateMap<UserRequest, Users>();
            CreateMap<UsersDetails, UserDetailsResponse>();

            CreateMap<Companies, CompanyResponse>();
            CreateMap<CompanyRequest, Companies>();


            CreateMap<Offices, OfficeResponse>();
            CreateMap<OfficeRequest, Offices>();

            CreateMap<Profiles, ProfileResponse>();
            CreateMap<ProfileRequest, Profiles>();
            CreateMap<Global.Requests.Permissions, ProfilesDetails>();
            CreateMap<ProfilesDetails, Global.Responses.Permissions>();

            CreateMap<Projects, ProjectResponse>();
            CreateMap<ProjectRequest, Projects>();

            CreateMap<Programs, ProgramResponse>();
            CreateMap<ProgramRequest, Programs>();

            CreateMap<Concepts, ConceptResponse>();
            CreateMap<ConceptRequest, Concepts>();

            CreateMap<PaymentMethods, PaymentMethodResponse>();
            CreateMap<PaymentMethodRequest, PaymentMethods>();

            CreateMap<Agents, AgentResponse>();
            CreateMap<AgentRequest, Agents>();

            CreateMap<Vendors, VendorResponse>();
            CreateMap<VendorRequest, Vendors>();

            CreateMap<Customers, CustomerResponse>();
            CreateMap<CustomerRequest, Customers>();

            CreateMap<Costs, CostResponse>();
            CreateMap<CostRequest, Costs>();

            CreateMap<Departments, DepartmentResponse>();
            CreateMap<DepartmentRequest, Departments>();

            CreateMap<Sales, SaleResponse>();
            CreateMap<SaleRequest, Sales>();
            CreateMap<SalesDetails, SaleDetailsResponse>();

            CreateMap<Expenses, ExpenseResponse>();
            CreateMap<ExpenseRequest, Expenses>();
            CreateMap<ExpensesDetails, ExpenseDetailsResponse>();

            CreateMap<Budgets, BudgetResponse>();
            CreateMap<BudgetRequest, Budgets>();
            CreateMap<BudgetsDetails, BudgetDetailsResponse>();
        }
    }
}
