﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class SalesService : ISalesService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public SalesService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<SaleResponse> GetSales()
        {
            var sales = _context.Sales.ToList();

            if (sales.Count == 0)
            {
                return null;
            }

            var saleResponses = new List<SaleResponse>();

            foreach (var sale in sales)
            {
                var company = _context.Companies.Where(x => x.Id == sale.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == sale.OfficeId).FirstOrDefault();
                var salesDetails = _context.SalesDetails.Where(x => x.SaleId == sale.Id).OrderBy(x => x.PaidDate).ThenBy(x => x.StatusId == 0).Select(x => _mapper.Map<SaleDetailsResponse>(x)).ToList();
                var customerName = _context.Customers.Where(x => x.Id == sale.CustomerId).Select(x => x.Name).FirstOrDefault();
                var unitName = _context.Departments.Where(x => x.Id == sale.DepartmentId).Select(x => x.Unit).FirstOrDefault();
                var saleResponse = _mapper.Map<SaleResponse>(sale);
                saleResponse.Sales = salesDetails;
                saleResponse.CustomerName = customerName;
                saleResponse.UnitName = unitName;

                saleResponses.Add(saleResponse);
            }

            return saleResponses;
        }

        public SaleResponse GetSale(Guid id)
        {
            var sale = _context.Sales.Where(x => x.Id == id).FirstOrDefault();

            if (sale == null)
            {
                return null;
            }

            return _mapper.Map<SaleResponse>(sale);
        }

        public async Task<SaleResponse> SaveSale(SaleRequest request)
        {
            var dbRequest = _mapper.Map<Sales>(request);
            await _context.AddAsync(dbRequest);

            if (request.SaleDetails.Count > 0)
            {
                foreach (var saleDetail in request.SaleDetails)
                {
                    var salesDetails = new SalesDetails()
                    {
                        SaleId = dbRequest.Id,
                        PaidDate = saleDetail.PaidDate,
                        PaidImport = saleDetail.PaidImport,
                        ConceptId = saleDetail.ConceptId,
                        PaymentMethodId = saleDetail.PaymentMethodId,
                        StatusId = saleDetail.StatusId
                    };

                    await _context.SalesDetails.AddAsync(salesDetails);
                }
            }

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<SaleResponse>(dbRequest);
            }

            return null;
        }

        public async Task<SaleResponse> UpdateSale(SaleRequest request)
        {
            var sale = _context.Sales.Where(x => x.Id == request.Id).FirstOrDefault();

            if (sale == null)
            {
                throw new Exception($"No sale was found with id: {request.Id}");
            }

            sale.CompanyId = request.CompanyId;
            sale.OfficeId = request.OfficeId;
            sale.ProjectId = request.ProjectId;
            sale.DepartmentId = request.DepartmentId;
            sale.ProgramId = request.ProgramId;
            sale.ContractDate = request.ContractDate;
            sale.CustomerId = request.CustomerId;
            sale.Scheme = request.Scheme;
            sale.SalePrice = request.SalePrice;
            sale.Paid = request.Paid;
            sale.Residue = request.Residue;
            sale.Percentage = request.Percentage;
            sale.AgentId = request.AgentId;
            sale.PercentageAgent = request.PercentageAgent;
            sale.ComisionAgent = request.ComisionAgent;
            sale.ModifUser = "string";
            sale.ModifDate = DateTime.Now;

            var salesDetailsList = _context.SalesDetails.Where(x => x.SaleId == sale.Id).ToList();
            _context.SalesDetails.RemoveRange(salesDetailsList);

            if (request.SaleDetails.Count > 0)
            {
                foreach (var saleDetail in request.SaleDetails)
                {
                    var salesDetails = new SalesDetails()
                    {
                        SaleId = sale.Id,
                        PaidDate = saleDetail.PaidDate,
                        PaidImport = saleDetail.PaidImport,
                        ConceptId = saleDetail.ConceptId,
                        PaymentMethodId = saleDetail.PaymentMethodId,
                        StatusId = saleDetail.StatusId
                    };

                    _context.SalesDetails.Add(salesDetails);
                }
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<SaleResponse>(sale);
        }

        public async Task<bool?> DeleteSale(Guid Id)
        {
            var sale = _context.Sales.Where(x => x.Id == Id).FirstOrDefault();

            if (sale == null)
            {
                return false;
            }

            _context.Sales.Remove(sale);
            var salesDetails = _context.SalesDetails.Where(x => x.SaleId == Id).ToList();
            _context.SalesDetails.RemoveRange(salesDetails);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
