﻿using AutoMapper;
using Global.Requests;
using Global.Responses;
using Model.DB;
using Model.DB.Tables.Interfaces;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ProgramsService : IProgramsService
    {
        private readonly NautilusDB _context;
        private readonly IMapper _mapper;

        public ProgramsService(NautilusDB context,
           IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<ProgramResponse> GetPrograms()
        {
            var programs = _context.Programs.ToList();

            if (programs.Count == 0)
            {
                return null;
            }

            var programsResponse = new List<ProgramResponse>();

            foreach (var program in programs)
            {
                var company = _context.Companies.Where(x => x.Id == program.CompanyId).FirstOrDefault();
                var office = _context.Offices.Where(x => x.Id == program.OfficeId).FirstOrDefault();
                var programResponse = _mapper.Map<ProgramResponse>(program);
                programResponse.CompanyName = company?.Name;
                programResponse.OfficeName = office?.Name;
                programsResponse.Add(programResponse);
            }

            return programsResponse;
        }

        public ProgramResponse GetProgram(Guid id)
        {
            var program = _context.Programs.Where(x => x.Id == id).FirstOrDefault();

            if (program == null)
            {
                return null;
            }

            return _mapper.Map<ProgramResponse>(program);
        }

        public async Task<ProgramResponse> SaveProgram(ProgramRequest request)
        {
            var dbRequest = _mapper.Map<Programs>(request);
            await _context.AddAsync(dbRequest);

            if (await _context.SaveChangesAsync() > 0)
            {
                return _mapper.Map<ProgramResponse>(dbRequest);
            }

            return null;
        }

        public async Task<ProgramResponse> UpdateProgram(ProgramRequest request)
        {
            var program = _context.Programs.Where(x => x.Id == request.Id).FirstOrDefault();

            if (program == null)
            {
                throw new Exception($"No program was found with id: {request.Id}");
            }

            program.CompanyId = request.CompanyId;
            program.OfficeId = request.OfficeId;
            program.Name = request.Name;
            program.ModifUser = "string";
            program.ModifDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return _mapper.Map<ProgramResponse>(program);
        }

        public async Task<bool?> DeleteProgram(Guid Id)
        {
            var program = _context.Programs.Where(x => x.Id == Id).FirstOrDefault();

            if (program == null)
            {
                return false;
            }

            _context.Programs.Remove(program);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
